<?php
/**
 * The template for displaying all pages, single posts and attachments
 *
 * @package Total WordPress Theme
 * @subpackage Templates
 * @version 5.0.6
 */

defined( 'ABSPATH' ) || exit;

get_header();

$bgColor = get_field('background_colour');
$img = get_field('main_image');

$secondary = '#004053';
$altSecondary = get_field('secondary_colour');
if($altSecondary){
	$secondary = $altSecondary;
}

?>

<style>
figure.wp-block-table table thead {
	background-color: <?php echo $secondary; ?>;
}
</style>

	<div id="content-wrap" class="container wpex-clr" style="margin-bottom: -30px; background-color: <?php echo $bgColor; ?>;">

		<?php wpex_hook_primary_before(); ?>

		<div class="animal_experiences_single">
			<div class="container p-0 mb-3">
				<div class="row">
					<div class="col-12 col-md-6 col-xl-7">
						<div class="content_left">
							<div class="d-none d-md-block d-xl-none">
								<?php wpex_get_template_part( 'breadcrumbs' ); ?>
							</div>

							<div class=" d-none d-md-block previous_page">
								<a href="/wild-gifts-and-experiences/animal-experiences/">
									<span class="fa-stack fa-1x">
				                        <i style="color: <?php echo $secondary; ?>;" class="fas fa-circle fa-stack-1x"></i>
				                        <i class="fas fa-angle-left fa-stack-1x fa-inverse"></i>
				                    </span>
									<span style="color: <?php echo $secondary; ?>;" class="prev_page_label">Back to Animal Experiences</span>
								</a>
							</div>

							<h1 class="d-block d-xl-none"><?php echo get_the_title(); ?></h1>

							<div class="content_image">
								<svg class="svg_top" viewBox="0 0 1569 34" version="1.1">
									<g transform="rotate(180,784.5,17.04549)" style="fill:<?php echo $bgColor; ?>;fill-opacity:1">
										<path fill="<?php echo $bgColor; ?>" style="fill:<?php echo $bgColor; ?>;fill-opacity:1" d="M 210.393,0 0,10.295 v 5.144 324.249 11.765 l 161.466,-6.62 104.381,5.15 192.46,-5.15 156.569,5.15 550.456,1.47 99.49,-11.765 304.178,5.145 V 5.145 L 1352.895,10.295 1268.898,8.09 1071.553,4.41 998.16,10.295 959.014,4.41 503.159,13.969 344.138,5.145 Z" />
									</g>
								</svg>
								<img src="<?php echo $img['url']; ?>" />
								<svg class="svg_bottom" viewBox="0 0 1569 34" version="1.1">
									<g transform="rotate(180,784.5,17.04549)" style="fill:<?php echo $bgColor; ?>;fill-opacity:1">
										<path fill="<?php echo $bgColor; ?>" style="fill:<?php echo $bgColor; ?>;fill-opacity:1" d="M 210.393,0 0,10.295 v 5.144 324.249 11.765 l 161.466,-6.62 104.381,5.15 192.46,-5.15 156.569,5.15 550.456,1.47 99.49,-11.765 304.178,5.145 V 5.145 L 1352.895,10.295 1268.898,8.09 1071.553,4.41 998.16,10.295 959.014,4.41 503.159,13.969 344.138,5.145 Z" />
									</g>
								</svg>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-xl-5">
						<div class="content_right">
							<div class="svg_top">
								<svg viewBox="0 0 459.00001 26.47788" version="1.1">
									<path fill="<?php echo $secondary; ?>" class="a" d="m 290.543,1045.457 -26.673,-10.157 -3.4,1.209 -19.989,-1.029 -2.606,1.029 -18.655,-0.859 -36.1,12.908 -8.769,-4.987 -13.765,4.987 -38.7,-21.856 H 104.72 l -37.584,4.474 -40.553,10.848 v -6.027 L 4.042,1042.024 3.829,997.831 H 0 V 33.482 h 6.666 l 9.064,-3.924 28.814,-10.77 13.514,-0.188 9.078,0.183 26.791,-1.993 27.956,1.31 15.582,1.205 6.962,-1.205 38.694,-1.1 2.65,-1.048 21.542,3.288 1.887,-0.64 40.7,1.72 40.643,-2.22 40.648,10.137 14.073,0.064 8.594,-1.61 18.181,0.862 4.353,-0.862 7.923,0.358 28.22,-3.108 35.234,9.542 H 459 v 964.345 h -6.732 l -1.35,29.219 -7.707,-1.906 -30.676,22.73 -40.645,-6.2 -40.642,6.881 z" transform="rotate(180,229.5,21.214939)" clip-path="url(#clipPath839)" />
								</svg>
							</div>
							<div style="background-color: <?php echo $secondary; ?>;" class="content_right_inner">
								<div class="d-none d-xl-block">
									<?php wpex_get_template_part( 'breadcrumbs' ); ?>
								</div>
								<h1 class="d-none d-xl-block" style="color: <?php echo $bgColor; ?>"><?php echo get_the_title(); ?></h1>
								<div class="intro_content"><?php echo get_field('intro_content'); ?></div>
							</div>
							<div class="svg_bottom">
								<svg viewBox="0 0 459.00001 26.47788" version="1.1">
									<path fill="<?php echo $secondary; ?>" class="a" d="m 290.543,1045.457 -26.673,-10.157 -3.4,1.209 -19.989,-1.029 -2.606,1.029 -18.655,-0.859 -36.1,12.908 -8.769,-4.987 -13.765,4.987 -38.7,-21.856 H 104.72 l -37.584,4.474 -40.553,10.848 v -6.027 L 4.042,1042.024 3.829,997.831 H 0 V 33.482 h 6.666 l 9.064,-3.924 28.814,-10.77 13.514,-0.188 9.078,0.183 26.791,-1.993 27.956,1.31 15.582,1.205 6.962,-1.205 38.694,-1.1 2.65,-1.048 21.542,3.288 1.887,-0.64 40.7,1.72 40.643,-2.22 40.648,10.137 14.073,0.064 8.594,-1.61 18.181,0.862 4.353,-0.862 7.923,0.358 28.22,-3.108 35.234,9.542 H 459 v 964.345 h -6.732 l -1.35,29.219 -7.707,-1.906 -30.676,22.73 -40.645,-6.2 -40.642,6.881 z" transform="rotate(180,229.5,21.214939)" clip-path="url(#clipPath839)" />
								</svg>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="primary" class="content-area wpex-clr">

				<?php wpex_hook_content_before(); ?>

				<div id="content" class="site-content wpex-clr">

					<?php wpex_hook_content_top(); ?>

					<div class="container p-0">

						<?php
						// Display singular content unless there is a custom template defined
						if ( ! wpex_theme_do_location( 'single' ) ) :

							// Start loop
							while ( have_posts() ) : the_post();

							// Single Page
							if ( is_singular( 'page' ) ) {

								wpex_get_template_part( 'page_single_blocks' );

							}

							// Single posts
							elseif ( is_singular( 'post' ) ) {

								wpex_get_template_part( 'blog_single_blocks' );

							}

							// Portfolio Posts
							elseif ( is_singular( 'portfolio' ) && wpex_is_total_portfolio_enabled() ) {

								wpex_get_template_part( 'portfolio_single_blocks' );

							}

							// Staff Posts
							elseif ( is_singular( 'staff' ) && wpex_is_total_staff_enabled() ) {

								wpex_get_template_part( 'staff_single_blocks' );

							}

							// Testimonials Posts
							elseif ( is_singular( 'testimonials' ) && wpex_is_total_testimonials_enabled() ) {

								wpex_get_template_part( 'testimonials_single_blocks' );

							}

							/**
							* All other post types.
							*
							* When customizing your custom post types it's best to create
							* a new singular-{post_type}.php file to prevent any possible conflicts in the future
							* rather then altering the template part or create a dynamic template.
							*
							* @link https://wpexplorer-themes.com/total/docs/custom-post-type-singular-template/
							*/
							else {

								// Prevent issues with custom types named the same as core partial files.
								// @todo remove the $post_type paramater from wpex_get_template_part.
								$post_type = get_post_type();

								if ( in_array( $post_type, array( 'audio', 'video', 'gallery', 'content', 'comments', 'media', 'meta', 'related', 'share', 'title' ) ) ) {
									$post_type = null;
								}

								wpex_get_template_part( 'cpt_single_blocks', $post_type );

							}

							endwhile; ?>

						<?php endif; ?>

					</div>

					<?php wpex_hook_content_bottom(); ?>

				</div>

				<?php wpex_hook_content_after(); ?>

			</div>

		</div>

		<?php wpex_hook_primary_after(); ?>

	</div>

<?php get_footer(); ?>
