<?php
/*
 * Initialise custom cards for Total child theme
 */

function bwmCustomCards( $card_styles )
{
	$card_styles['nzoonews'] = array(
		'name' => esc_html__( 'Newquay Zoo News', 'Total' ),
	);
	return $card_styles;
}
add_filter( 'wpex_card_styles', 'bwmCustomCards' );
