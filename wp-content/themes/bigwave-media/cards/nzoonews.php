<?php
/**
 * Card: Blog 1
 *
 * @package Total WordPress Theme
 * @subpackage Cards
 * @version 5.0.5
 */

defined( 'ABSPATH' ) || exit;

$output = '';

// Media
$output .= $this->get_media( array(
	'class' => 'wpex-mb-20',
) );

$index = $this->get_running_count() % 4;


return '
<div class="text_image_alternate_block listings">
    <div class="text_image_grid">
        <div class="row no-gutters">
            <div class="col-12 col-md-6 col-xl-7 order-1 order-md-1">
                <div class="grid_item_image">
                    '.$this->get_media([
                        'class' => 'h-100'
                    ]).'
                </div>
            </div>
            <div class="accent order-2 order-md-2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 860 34.984">
                    <path class="a" style="fill: '.returnColoursArray($index).';" d="M309.034,34.984H213.405l-70.014-1.158-31.63,1.158H88.45L0,33.826,0,4.295,143.391,1.266l76.4,3.029L328.937,2.15l67.7,1.07L427.963,2.6,558.249,0,703.5,4.295,768.857,2.15,860,4.295V32.521L719.648,34.178l-99.889-.352-61.509.352-105.616-.352-63.961-1.3Z"/>
                </svg>
            </div>
            <div class="col-12 col-md-6 col-xl-5 order-2 order-md-2">
                <div class="grid_item_content pt-0 pt-md-4">

                    ' . $this->get_title() . '

                    <div class="item_content">

                        <div class="mb-3 d-flex flex-row justify-content-start card_blog_post_meta">

                            ' . $this->get_date([
                                'format' => 'j F Y',
                            ]) . '

                        </div>

                        ' . $this->get_excerpt([
                            'class' => 'mb-4'
                        ]) . '

                        ' . $this->get_more_link([
                            'link_class' => 'item_link',
                            'text' => esc_html__( 'READ MORE', 'total' )
                        ]).'

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
';


$output .= '<div class="mandem wpex-card-meta wpex-flex wpex-flex-wrap wpex-mb-15 wpex-child-inherit-color">';

	// Date
	$output .= $this->get_date( array(
		'class' => 'wpex-mr-20',
		'icon'  => 'ticon ticon-clock-o wpex-mr-5',
	) );

	// Author
	$output .= $this->get_author( array(
		'class' => 'wpex-mr-20',
		'icon' => 'ticon ticon-user-o wpex-mr-5',
	) );

	// Primary Term
	$output .= $this->get_primary_term( array(
		'class' => 'wpex-mr-20',
		'term_class' => 'wpex-mr-5',
		'icon' => 'ticon ticon-folder-o wpex-mr-5',
	) );

	// Comment Count
	$this->get_comment_count( array(
		'class' => 'wpex-child-inherit-color',
		'icon' => 'ticon ticon-comment-o wpex-mr-5',
	) );

$output .= '</div>';

// Excerpt
$output .= $this->get_excerpt( array(
	'class' => 'wpex-mb-20',
) );

// More Button
$output .= $this->get_more_link( array(
	'link_class' => 'theme-button',
	'text'       => esc_html__( 'Read more', 'total' ),
) );

return $output;
