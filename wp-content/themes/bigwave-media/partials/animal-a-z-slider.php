
<script type="text/x-template" id="animalsAZSlider">
    <div class="animal_a_z">

        <div class="a_z_controls">
            <carousel
            navigationNextLabel="<i class='fas fa-angle-right'></i>"
            navigationPrevLabel="<i class='fas fa-angle-left'></i>"
            :navigationEnabled="true"
            :paginationEnabled="false"
            :perPageCustom="[[360, 3], [480, 4], [768, 6], [1200, 7]]"
            :perPage="3">
                <slide v-for="letter in alphabet">
                    <a
                    :href="'/explore/animals/?letter=' + letter "
                    v-html="letter.toUpperCase()"
                    ></a>
                </slide>
            </carousel>
        </div>

    </div>
</script>
