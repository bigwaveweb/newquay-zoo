<?php
$facebook = get_field('facebook', 'option');
$twitter = get_field('twitter', 'option');
$youtube = get_field('youtube', 'option');
$instagram = get_field('instagram', 'option');
?>

<ul class="social">
    <?php if($facebook){ ?>
        <li><a target="_blank" href="<?php echo $facebook; ?>"><div class="social_item">
            <svg class="social_media_svg" xmlns="http://www.w3.org/2000/svg" width="27.322" height="27.322" viewBox="0 0 27.322 27.322">
                <path class="social_footer_menu" d="M13.441,0,9.816.488,6.19,2.179,3.407,3.939,2.125,6.493.2,10.318,0,13.8l.473,3.417.912,3.428L3.58,23.5l3.233,1.589,3.06,1.8,3.568-.011,3.684.443,3.741-1.759,2.217-2.27,1.825-2.974,1.825-3.008.589-3.507-.15-3.621-1.64-3.576-2.656-2.1L20.024,2.565,16.987.806,13.545.023Z" transform="translate(27.322) rotate(90)"></path>
            </svg>
            <i class="fab fa-facebook-f"></i>
        </div></a></li>
    <?php } ?>
    <?php if($twitter){ ?>
        <li><a target="_blank" href="<?php echo $twitter; ?>"><div class="social_item">
            <svg class="social_media_svg" xmlns="http://www.w3.org/2000/svg" width="27.322" height="27.322" viewBox="0 0 27.322 27.322">
                <path class="social_footer_menu" d="M13.441,0,9.816.488,6.19,2.179,3.407,3.939,2.125,6.493.2,10.318,0,13.8l.473,3.417.912,3.428L3.58,23.5l3.233,1.589,3.06,1.8,3.568-.011,3.684.443,3.741-1.759,2.217-2.27,1.825-2.974,1.825-3.008.589-3.507-.15-3.621-1.64-3.576-2.656-2.1L20.024,2.565,16.987.806,13.545.023Z" transform="translate(27.322) rotate(90)"></path>
            </svg>
            <i class="fab fa-twitter"></i>
        </div></a></li>
    <?php } ?>
    <?php if($youtube){ ?>
        <li><a target="_blank" href="<?php echo $youtube; ?>"><div class="social_item">
            <svg class="social_media_svg" xmlns="http://www.w3.org/2000/svg" width="27.322" height="27.322" viewBox="0 0 27.322 27.322">
                <path class="social_footer_menu" d="M13.441,0,9.816.488,6.19,2.179,3.407,3.939,2.125,6.493.2,10.318,0,13.8l.473,3.417.912,3.428L3.58,23.5l3.233,1.589,3.06,1.8,3.568-.011,3.684.443,3.741-1.759,2.217-2.27,1.825-2.974,1.825-3.008.589-3.507-.15-3.621-1.64-3.576-2.656-2.1L20.024,2.565,16.987.806,13.545.023Z" transform="translate(27.322) rotate(90)"></path>
            </svg>
            <i class="fab fa-youtube"></i>
        </div></a></li>
    <?php } ?>
    <?php if($instagram){ ?>
        <li><a target="_blank" href="<?php echo $instagram; ?>"><div class="social_item">
            <svg class="social_media_svg" xmlns="http://www.w3.org/2000/svg" width="27.322" height="27.322" viewBox="0 0 27.322 27.322">
                <path class="social_footer_menu" d="M13.441,0,9.816.488,6.19,2.179,3.407,3.939,2.125,6.493.2,10.318,0,13.8l.473,3.417.912,3.428L3.58,23.5l3.233,1.589,3.06,1.8,3.568-.011,3.684.443,3.741-1.759,2.217-2.27,1.825-2.974,1.825-3.008.589-3.507-.15-3.621-1.64-3.576-2.656-2.1L20.024,2.565,16.987.806,13.545.023Z" transform="translate(27.322) rotate(90)"></path>
            </svg>
            <i class="fab fa-instagram"></i>
        </div></a></li>
    <?php } ?>
</ul>
