<?php $img = apply_filters( 'wpex_header_logo_img_url', wpex_get_translated_theme_mod( 'custom_logo' ) ); ?>

<div class="footer_other_sites">
    <a target="_blank" class="other_sites wpt" href="https://www.wildplanettrust.org.uk/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/wildplanettrust-logo.png" loading="lazy" /></a>
    <a class="other_sites central d-block d-md-none" href="/">
        <img src="<?php echo wpex_get_image_url( $img ); ?>" />
    </a>
    <a target="_blank" class="other_sites" href="https://www.paigntonzoo.org.uk/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/paignton-zoo-logo.png" loading="lazy" /></a>
</div>
