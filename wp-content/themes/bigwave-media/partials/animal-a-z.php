

<script type="text/x-template" id="animalsAZ">
    <div class="animal_a_z">

        <transition-group name="fade" mode="out-in" tag="div">
            <div key="not_loaded" v-if="!loaded" class="loading">
                <div class="svg_wrapper svg_top">
                    <svg viewBox="0 0 459.00001 26.47788" version="1.1">
                        <path fill="#004053" d="m 290.543,1045.457 -26.673,-10.157 -3.4,1.209 -19.989,-1.029 -2.606,1.029 -18.655,-0.859 -36.1,12.908 -8.769,-4.987 -13.765,4.987 -38.7,-21.856 H 104.72 l -37.584,4.474 -40.553,10.848 v -6.027 L 4.042,1042.024 3.829,997.831 H 0 V 33.482 h 6.666 l 9.064,-3.924 28.814,-10.77 13.514,-0.188 9.078,0.183 26.791,-1.993 27.956,1.31 15.582,1.205 6.962,-1.205 38.694,-1.1 2.65,-1.048 21.542,3.288 1.887,-0.64 40.7,1.72 40.643,-2.22 40.648,10.137 14.073,0.064 8.594,-1.61 18.181,0.862 4.353,-0.862 7.923,0.358 28.22,-3.108 35.234,9.542 H 459 v 964.345 h -6.732 l -1.35,29.219 -7.707,-1.906 -30.676,22.73 -40.645,-6.2 -40.642,6.881 z" transform="rotate(180,229.5,21.214939)" clip-path="url(#clipPath839)" />
                    </svg>
                </div>
                <div class="loading_inner">
                    <h2>Finding Animals</h2>
                    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                </div>
                <div class="svg_wrapper svg_bottom">
                    <svg viewBox="0 0 459.00001 26.47788" version="1.1">
                        <path fill="#004053" d="m 290.543,1045.457 -26.673,-10.157 -3.4,1.209 -19.989,-1.029 -2.606,1.029 -18.655,-0.859 -36.1,12.908 -8.769,-4.987 -13.765,4.987 -38.7,-21.856 H 104.72 l -37.584,4.474 -40.553,10.848 v -6.027 L 4.042,1042.024 3.829,997.831 H 0 V 33.482 h 6.666 l 9.064,-3.924 28.814,-10.77 13.514,-0.188 9.078,0.183 26.791,-1.993 27.956,1.31 15.582,1.205 6.962,-1.205 38.694,-1.1 2.65,-1.048 21.542,3.288 1.887,-0.64 40.7,1.72 40.643,-2.22 40.648,10.137 14.073,0.064 8.594,-1.61 18.181,0.862 4.353,-0.862 7.923,0.358 28.22,-3.108 35.234,9.542 H 459 v 964.345 h -6.732 l -1.35,29.219 -7.707,-1.906 -30.676,22.73 -40.645,-6.2 -40.642,6.881 z" transform="rotate(180,229.5,21.214939)" clip-path="url(#clipPath839)" />
                    </svg>
                </div>
            </div>

            <div key="loaded" v-else class="animals">

                <div v-if="loadedSuccess">

                    <div class="a_z_controls">
                        <carousel
                        navigationNextLabel="<i class='fas fa-angle-right'></i>"
                        navigationPrevLabel="<i class='fas fa-angle-left'></i>"
                        :navigationEnabled="true"
                        :paginationEnabled="false"
                        :perPageCustom="[[360, 4], [480, 6], [768, 9]]"
                        :perPage="3">
                            <slide v-for="(letter, index) in alphabet">
                                <button :class="currentLetter == letter ? 'active' : false" @click="currentLetter = letter" v-html="letter.toUpperCase()"></button>
                            </slide>
                        </carousel>
                    </div>

                    <transition-group mode="out-in" class="row no-gutters" tag="div" name="sessions" v-if="animals">
                        <div v-for="(a, i) in filteredAnimals" :key="a.post_title" class="col-12 col-md-6 col-xl-4">
                            <div class="animal_item">
                                <a :href="a.wp_link">
                                    <div class="animal_image" v-html="a.featured_image"></div>
                                    <div class="animal_title" :style="{ 'background-color': getColourClass(i) }">
                                        <h3 v-html="a.post_title"></h3>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div v-if="filteredAnimals.length < 1" key="no_animals" class="no_animals_found">
                            <div class="svg_wrapper svg_top">
                                <svg viewBox="0 0 459.00001 26.47788" version="1.1">
                                    <path fill="#004053" d="m 290.543,1045.457 -26.673,-10.157 -3.4,1.209 -19.989,-1.029 -2.606,1.029 -18.655,-0.859 -36.1,12.908 -8.769,-4.987 -13.765,4.987 -38.7,-21.856 H 104.72 l -37.584,4.474 -40.553,10.848 v -6.027 L 4.042,1042.024 3.829,997.831 H 0 V 33.482 h 6.666 l 9.064,-3.924 28.814,-10.77 13.514,-0.188 9.078,0.183 26.791,-1.993 27.956,1.31 15.582,1.205 6.962,-1.205 38.694,-1.1 2.65,-1.048 21.542,3.288 1.887,-0.64 40.7,1.72 40.643,-2.22 40.648,10.137 14.073,0.064 8.594,-1.61 18.181,0.862 4.353,-0.862 7.923,0.358 28.22,-3.108 35.234,9.542 H 459 v 964.345 h -6.732 l -1.35,29.219 -7.707,-1.906 -30.676,22.73 -40.645,-6.2 -40.642,6.881 z" transform="rotate(180,229.5,21.214939)" clip-path="url(#clipPath839)" />
                                </svg>
                            </div>
                            <div class="no_animals_found_inner">
                                <h3><span v-html="currentLetter.toUpperCase()"></span>: No Animals Found...</h3>
                            </div>
                            <div class="svg_wrapper svg_bottom">
                                <svg viewBox="0 0 459.00001 26.47788" version="1.1">
                                    <path fill="#004053" d="m 290.543,1045.457 -26.673,-10.157 -3.4,1.209 -19.989,-1.029 -2.606,1.029 -18.655,-0.859 -36.1,12.908 -8.769,-4.987 -13.765,4.987 -38.7,-21.856 H 104.72 l -37.584,4.474 -40.553,10.848 v -6.027 L 4.042,1042.024 3.829,997.831 H 0 V 33.482 h 6.666 l 9.064,-3.924 28.814,-10.77 13.514,-0.188 9.078,0.183 26.791,-1.993 27.956,1.31 15.582,1.205 6.962,-1.205 38.694,-1.1 2.65,-1.048 21.542,3.288 1.887,-0.64 40.7,1.72 40.643,-2.22 40.648,10.137 14.073,0.064 8.594,-1.61 18.181,0.862 4.353,-0.862 7.923,0.358 28.22,-3.108 35.234,9.542 H 459 v 964.345 h -6.732 l -1.35,29.219 -7.707,-1.906 -30.676,22.73 -40.645,-6.2 -40.642,6.881 z" transform="rotate(180,229.5,21.214939)" clip-path="url(#clipPath839)" />
                                </svg>
                            </div>
                        </div>
                    </transition-group>

                </div>
                <div v-else>
                    <h2>Could not find animals</h2>
                </div>
            </div>
        </transition-group>

    </div>
</script>
