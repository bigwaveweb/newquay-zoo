<form class="newsletter_signup" action="/newsletter-signup/">
    <label>Sign up to our newsletter</label>
    <div class="w-100 d-flex flex-column flex-md-row align-items-center align-items-md-start">
        <input type="email" name="email" placeholder="Email Address" />
        <div class="form_submit">
            <button type="submit">Subscribe</button>
            <img src="/wp-content/themes/bigwave-media/assets/form-bg.svg" />
        </div>
    </div>
</form>
