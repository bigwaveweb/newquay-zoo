<?php

flush_rewrite_rules(true);

add_action('init', 'customRSS');
function customRSS()
{
    add_feed('zoo-news', 'newquayZooNewsRSS');
    add_feed('zoo-blog', 'newquayZooBlogRSS');
    add_feed('zoo-events', 'newquayZooEventsRSS');
}

function newquayZooNewsRSS()
{
    get_template_part('partials/feeds/rss-feed-news');
}

function newquayZooEventsRSS()
{
    get_template_part('partials/feeds/rss-feed-events');
}

function newquayZooBlogRSS()
{
    get_template_part('partials/feeds/rss-feed-blog');
}
