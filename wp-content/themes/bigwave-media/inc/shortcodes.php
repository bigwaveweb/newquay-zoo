<?php

add_shortcode('newsletter-signup', 'newsletterSignupShortcode');
/* function newsletterSignupShortcode()
{
    $email = $_GET['email'];

    $date = date('d/m/Y');

    return <<<HTML
    <div class="newsletter_signup_wrapper">

        <script src="https://www.google.com/recaptcha/api.js"></script>
        <script>
            function timestamp() {
                var response = document.getElementById("g-recaptcha-response");
                if (response == null || response.value.trim() == "") {
                    var elems = JSON.parse(document.getElementsByName("captcha_settings")[0].value);
                    elems["ts"] = JSON.stringify(new Date().getTime());
                    document.getElementsByName("captcha_settings")[0].value = JSON.stringify(elems);
                }
            }
            setInterval(timestamp, 500);
        </script>

        <form action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST" class="signup-form newsletter_signup" id="subForm">

            <input type=hidden name='captcha_settings' value='{"keyname":"NewPZ","fallback":"true","orgId":"00D0X000000uYzs","ts":""}'>
            <input type=hidden name="oid" value="00D0X000000uYzs">
            <input type=hidden name="retURL" value="https://cloud.wildplanettrust.email/signupconfirmation">

            <!--  ----------------------------------------------------------------------  -->
            <!--  NOTE: These fields are optional debugging elements. Please uncomment    -->
            <!--  these lines if you wish to test in debug mode.                          -->
            <!--  <input type="hidden" name="debug" value=1>                              -->
            <!--  <input type="hidden" name="debugEmail"                                  -->
            <!--  value="kat.sobey@wildplanettrust.org.uk">                               -->
            <!--  ----------------------------------------------------------------------  -->

            <div class="input-group" style="margin-top: 0px;">

                <h4 class="form-title my-2">YOUR DETAILS</h4>
                <label for="first_name">First Name</label>
                <input placeholder="First Name*" required id="first_name" maxlength="40" name="first_name" size="20" type="text" />

                <label for="last_name">Last Name</label>
                <input placeholder="Last Name*" required id="last_name" maxlength="80" name="last_name" size="20" type="text" />

                <label for="email">Email</label>
                <input placeholder="Email*" required value="$email" id="email" maxlength="80" name="email" size="20" type="text" />

                <input id="lead_source" name="lead_source" type="hidden" value="Website Newquay" />

                <input id="00N0X00000CnO2G" name="00N0X00000CnO2G" type="hidden" value="1" />
                <input id="00N0X00000CnO2Q" name="00N0X00000CnO2Q" type="hidden" value="1" />
                <input id="00N0X00000CnO7B" name="00N0X00000CnO7B" type="hidden" value="1" />

                <span class="dateInput dateOnlyInput">
                    <input id="00N0X00000CnO7L" name="00N0X00000CnO7L" size="12" type="hidden" value="$date" />
                </span>

                <input id="00N0X00000CnO7Q" name="00N0X00000CnO7Q" type="hidden" value="Online" title="Opt in method" />
                <input id="00N0X00000CnO7V" maxlength="255" name="00N0X00000CnO7V" value="newquayzoo.org.uk" type="hidden" />
                <input id="00N1x000006iU0x" name="00N1x000006iU0x" type="hidden" value="1" />
                <input id="00N1x000007q2Vr" name="00N1x000007q2Vr" type="hidden" value="1" />

                <label>Date of birth:</label>
                <span class="d-block w-100 dateInput dateOnlyInput">
                    <input id="00N1x000007q3XF" name="00N1x000007q3XF" size="12" type="text" />
                </span>

            </div>

            <div class="my-3">
                <div class="g-recaptcha" data-sitekey="6Le5ps8aAAAAAMfzOQ7sU3hDNEIITVdiwrNXi83E"></div>
            </div>

            <div class="input-group-btn">
                <input type="submit" name="submit">
            </div>

        </form>


    </div>
    HTML;

} */
function newsletterSignupShortcode()
{
    $email = $_GET['email'];

    $date = date('d/m/Y');

    return <<<HTML
    <div class="newsletter_signup_wrapper">

        <!--  ----------------------------------------------------------------------  -->
        <!--  NOTE: Please add the following <META> element to your page <HEAD>.      -->
        <!--  If necessary, please modify the charset parameter to specify the        -->
        <!--  character set of your HTML page.                                        -->
        <!--  ----------------------------------------------------------------------  -->

        <META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8">
        <script src="https://www.google.com/recaptcha/api.js"></script>
        <script>
         function timestamp() { var response = document.getElementById("g-recaptcha-response"); if (response == null || response.value.trim() == "") {var elems = JSON.parse(document.getElementsByName("captcha_settings")[0].value);elems["ts"] = JSON.stringify(new Date().getTime());document.getElementsByName("captcha_settings")[0].value = JSON.stringify(elems); } } setInterval(timestamp, 500);
        </script>

        <!--  ----------------------------------------------------------------------  -->
        <!--  NOTE: Please add the following <FORM> element to your page.             -->
        <!--  ----------------------------------------------------------------------  -->

        <form action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST" class="signup-form newsletter_signup" id="subForm">

            <input type=hidden name='captcha_settings' value='{"keyname":"NewPZ","fallback":"true","orgId":"00D0X000000uYzs","ts":""}'>
            <input type=hidden name="oid" value="00D0X000000uYzs">
            <input type=hidden name="retURL" value="https://cloud.wildplanettrust.email/signupconfirmation">

            <!--  ----------------------------------------------------------------------  -->
            <!--  NOTE: These fields are optional debugging elements. Please uncomment    -->
            <!--  these lines if you wish to test in debug mode.                          -->
            <!--  <input type="hidden" name="debug" value=1>                              -->
            <!--  <input type="hidden" name="debugEmail"                                  -->
            <!--  value="kat.sobey@wildplanettrust.org.uk">                               -->
            <!--  ----------------------------------------------------------------------  -->

            <div class="input-group" style="margin-top: 0px;">

                <h4 class="form-title my-2">YOUR DETAILS</h4>

                <label for="first_name">First Name</label>
                <input id="first_name" maxlength="40" placeholder="First Name" required name="first_name" size="20" type="text" />

                <label for="last_name">Last Name</label>
                <input id="last_name" maxlength="80" placeholde="Last Name" required name="last_name" size="20" type="text" />

                <label for="email">Email</label>
                <input id="email" maxlength="80" placeholder="Email" value="$email" required name="email" size="20" type="text" />

                <input id="lead_source" name="lead_source" value="Newquay Zoo website" type="hidden" />

                <input id="00N0X00000CnO2G" name="00N0X00000CnO2G" type="hidden" value="1" />
                <input id="00N0X00000CnO2Q" name="00N0X00000CnO2Q" type="hidden" value="1" />
                <input id="00N0X00000CnO7B" name="00N0X00000CnO7B" type="hidden" value="1" />

                <span class="dateInput dateOnlyInput">
                    <input id="00N0X00000CnO7L" name="00N0X00000CnO7L" size="12" type="hidden" value="$date" />
                </span>

                <input id="00N0X00000CnO7Q" name="00N0X00000CnO7Q" title="Opt in method" type="hidden" value="Online" />
                <input id="00N0X00000CnO7V" maxlength="255" name="00N0X00000CnO7V" size="20" type="hidden" value="https://newquayzoo.org.uk/" />

                <div style="visibility: hidden; position: absolute;">
                    <input id="00N5I0000032H4k" checked name="00N5I0000032H4k" type="checkbox" value="1" />
                    <input id="00N5I0000032IZj" checked name="00N5I0000032IZj" type="checkbox" value="1" />
                </div>

                <label>Date of Birth</label>
                <span class="dateInput dateOnlyInput w-100">
                    <input id="00N5I0000032IZo" name="00N5I0000032IZo" size="12" type="text" />
                </span>
            </div>

            <div class="my-3">
                <div class="g-recaptcha" data-sitekey="6Le5ps8aAAAAAMfzOQ7sU3hDNEIITVdiwrNXi83E"></div>
            </div>

            <input type="submit" name="submit">

        </form>

    </div>
    HTML;

}


function getSocialShareHTML()
{
    $colourString = '';
    if(is_page()){
        $colour = get_field('social_share_colour');
        $colourString = $colour ? $colour . '_bg' : '';
    }
	return '<div class="social_share ' . $colourString. '">
		<span class="share_heading">Share:</span>
		' . do_shortcode('[addtoany]') . '
	</div>';
}
add_shortcode('bwm-social-share', 'getSocialShareHTML');


add_shortcode('homepage-cta-strip', 'homepageCTA');
function homepageCTA()
{
    $html = '';

    $cta = '';
    $ctaItems = get_field('homepage_call_to_action');
    if($ctaItems){
        $cta .= '<div class="row">';
        foreach ($ctaItems as $key => $value) {
            $cta .= '
            <div class="col">
                <div class="cta_item">
                    <a href="'.$value['link']['url'].'" target="'.$value['link']['target'].'">
                        <img src="'.$value['image']['url'].'" />
                        <span>'.$value['link']['title'].'</span>
                    </a>
                </div>
            </div>
            ';
        }
        $cta .= '</div>';
    }

    $html .= '
    <div class="homepage_cta_wrapper">
        <div class="svg_wrapper svg_top">
            <svg preserveAspectRatio="none" style="isolation:isolate" viewBox="0 0 1569 34" version="1.1">
                <g clip-path="url(#clipPath842)" id="g9" transform="translate(0,-0.5726952)">
                    <path fill="#ffffff" id="path7" d="M 210.393,0 0,10.295 v 5.144 324.249 11.765 l 161.466,-6.62 104.381,5.15 192.46,-5.15 156.569,5.15 550.456,1.47 99.49,-11.765 304.178,5.145 V 5.145 L 1352.895,10.295 1268.898,8.09 1071.553,4.41 998.16,10.295 959.014,4.41 503.159,13.969 344.138,5.145 Z" />
                </g>
            </svg>
        </div>
        <div class="container">
            ' . $cta . '
        </div>
    </div>
    ';

    return $html;
}


function cmp($a, $b) {
    return $a['order_on_mobile'] > $b['order_on_mobile'];
}

function getHomepageGridItemHtml()
{
    return '
    <div class="grid_item %1$s">
        <div class="svg_wrapper svg_top d-block d-md-none">
            <svg preserveAspectRatio="none" style="isolation:isolate" viewBox="0 0 1569 34" version="1.1">
                <g clip-path="url(#clipPath842)" id="g9" transform="translate(0,-0.5726952)">
                    <path fill="%2$s" id="path7" d="M 210.393,0 0,10.295 v 5.144 324.249 11.765 l 161.466,-6.62 104.381,5.15 192.46,-5.15 156.569,5.15 550.456,1.47 99.49,-11.765 304.178,5.145 V 5.145 L 1352.895,10.295 1268.898,8.09 1071.553,4.41 998.16,10.295 959.014,4.41 503.159,13.969 344.138,5.145 Z" />
                </g>
            </svg>
        </div>
        <div class="item_content %3$s" style="background-color: %2$s;">
            <div class="item_text">
                <h3>%4$s</h3>
                <a href="%5$s" target="%6$s">
                    %7$s
                    <span class="link_arrow fa-stack fa-1x">
                        <i class="fas fa-circle fa-stack-1x"></i>
                        <i style="color: %8$s;" class="fas fa-angle-right fa-stack-1x fa-inverse"></i>
                    </span>
                </a>
            </div>
            %9$s
        </div>
    </div>
    ';
}

function getHomepageGridItemSocialHtml()
{
    $facebook = get_field('facebook', 'option');
    $twitter = get_field('twitter', 'option');
    $youtube = get_field('youtube', 'option');
    $instagram = get_field('instagram', 'option');

    return '
    <div class="social_item grid_item %1$s" style="background-color: %2$s;">
        <div class="item_content %3$s">
            <div class="item_text">
                <div class="social">
                    <div class="social_link"><a href="'.$facebook.'" target="_blank"><img src="/wp-content/themes/bigwave-media/assets/facebook.png" /></a></div>
                    <div class="social_link"><a href="'.$twitter.'" target="_blank"><img src="/wp-content/themes/bigwave-media/assets/twitter.png" /></a></div>
                    <div class="social_link"><a href="'.$youtube.'" target="_blank"><img src="/wp-content/themes/bigwave-media/assets/youtube.png" /></a></div>
                    <div class="social_link"><a href="'.$instagram.'" target="_blank"><img src="/wp-content/themes/bigwave-media/assets/instagram.png" /></a></div>
                </div>
                <h3>%4$s</h3>
            </div>
        </div>
    </div>
    ';
}

add_shortcode('homepage-grid', 'generateHomepageGrid');
if(!function_exists('generateHomepageGrid')){
    function generateHomepageGrid()
    {
        $html = '';
        $gridLeft = get_field('grid_items_left');
        $gridRight = get_field('grid_items_right');
        if(!$gridLeft || !$gridRight) {
            return;
        }

        $mobileArray = array_merge($gridLeft, $gridRight);
        usort($mobileArray, "cmp");

        $itemHTML = getHomepageGridItemHtml();

        $itemHTMLSocial = getHomepageGridItemSocialHtml();

        $html .= '<div class="grid_wrapper_container">';
            $html .= '<div class="d-none d-md-block grid_wrapper_left">';
                foreach ($gridLeft as $key => $item) {

                    $image = wp_get_attachment_image($item['image']['id'], 'large', false);

                    $string = $itemHTML;
                    if($item['replace_with_social']){
                        $string = $itemHTMLSocial;
                        $html .= sprintf(
                            $string,
                            $item['height'],
                            $item['background_colour'],
                            ' text_'.$item['alginment'].'_aligned',
                            $item['heading'],
                        );
                    } else {
                        $html .= sprintf(
                            $string,
                            $item['height'],
                            $item['background_colour'],
                            ' text_'.$item['alginment'].'_aligned',
                            $item['heading'],
                            $item['link']['url'],
                            $item['link']['target'],
                            $item['link']['title'],
                            $item['background_colour'],
                            $image,
                        );
                    }
                }
            $html .= '</div>';

            $html .= '<div class="d-none d-md-block grid_wrapper_right">';
            foreach ($gridRight as $key => $item) {

                $image = wp_get_attachment_image($item['image']['id'], 'large', false);

                $string = $itemHTML;
                if($item['replace_with_social']){
                    $string = $itemHTMLSocial;
                    $html .= sprintf(
                        $string,
                        $item['height'],
                        $item['background_colour'],
                        ' text_'.$item['alginment'].'_aligned',
                        $item['heading'],
                    );
                } else {
                    $html .= sprintf(
                        $string,
                        $item['height'],
                        $item['background_colour'],
                        ' text_'.$item['alginment'].'_aligned',
                        $item['heading'],
                        $item['link']['url'],
                        $item['link']['target'],
                        $item['link']['title'],
                        $item['background_colour'],
                        $image,
                    );
                }
            }
            $html .= '</div>';

            $html .= '<div class="d-block d-md-none grid_wrapper_left mobile">';
                foreach ($mobileArray as $key => $item) {

                    $image = wp_get_attachment_image($item['image']['id'], 'large', false);

                    if($item['replace_with_social']) continue;
                    $string = $itemHTML;
                    $html .= sprintf( $string,
                        $item['height'],
                        $item['background_colour'],
                        ' text_'.$item['alginment'].'_aligned',
                        $item['heading'],
                        $item['link']['url'],
                        $item['link']['target'],
                        $item['link']['title'],
                        $item['background_colour'],
                        $image,
                    );
                }
            $html .= '</div>';

        $html .= '</div>';


        return $html;
    }
}

function wpQueryParams($postType, $perPage, $paged)
{
    return array(
        'post_type' => $postType,
        'posts_per_page' => $perPage,
        'paged' => $paged
    );
}

add_shortcode('animal-experiences', 'animalExperiencesListings');
function animalExperiencesListings()
{
    // pagination setup
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    // args
    $args = wpQueryParams('animal-experiences', 6, $paged);
    // run da ting
    $the_query = new WP_Query( $args );

    // start output string
    $html = '';
    $i = 0;
    if ( $the_query->have_posts() ) {
        $sequentialColour = 0;
        $html .= '
        <div class="text_image_alternate_block accordion animal_experiences">
        <div class="text_image_grid">
        <div class="accordion_wrapper">
        <div class="row no-gutters-md">';
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                $img = get_the_post_thumbnail(get_the_id(), 'large');
                $html .= returnExperiencesHTML($img, $sequentialColour, $i);

                $i++;
                $sequentialColour++;
                if($sequentialColour > 2) {
                    $sequentialColour = 0;
                }
            }
        $html .= '</div></div></div></div>';
        $html .= fetchPagination($the_query->max_num_pages); // utilities
    } else {
        $html .= '<h2>No Posts Found</h2>';
    }
    wp_reset_postdata(); // reset

    return $html; // output
}

add_shortcode('post-archive', 'postArchiveListings');
function postArchiveListings($atts)
{

    $atts = shortcode_atts(array(
        'post_type' => 'post',
    ), $atts );

    // pagination setup
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    // args
    $args = wpQueryParams($atts['post_type'], 4, $paged);
    // run da ting
    $the_query = new WP_Query( $args );

    // start output string
    $html = '';
    if ( $the_query->have_posts() ) {
        $alternateLayoutSwitch = false;
        $sequentialColour = 0;
        $html .= '<div class="text_image_alternate_block listings"><div class="text_image_grid">';
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            $img = get_the_post_thumbnail_url(get_the_id(), 'full');
            $additionalContent = get_field('additional_content', get_the_id());
            $html .= returnListingsHTML($img, $alternateLayoutSwitch, $sequentialColour, $additionalContent);
            $alternateLayoutSwitch = !$alternateLayoutSwitch;
            $sequentialColour++;
            if($sequentialColour > 3) {
                $sequentialColour = 0;
            }
        }
        $html .= '</div></div>';
        $html .= '<div class="pagination">';
            $html .= fetchPagination($the_query->max_num_pages); // utilities
        $html .= '</div>';
    } else {
        $html .= '<h2>No Posts Found</h2>';
    }
    wp_reset_postdata(); // reset

    return $html; // output
}

add_shortcode('feeding-times', 'feedingTimesShortcode');
function feedingTimesShortcode($atts)
{

    $atts = shortcode_atts(array(
        'animal' => '',
    ), $atts );

    if(!$atts['animal']) return;
    $feedingTimes = get_field('feeding_times', 'option');
    if(!$feedingTimes) return;

    $html = '';

    $a = $atts['animal'];
    $filteredFeedingTimes = false;
    foreach ($feedingTimes as $v) {
        if($v['animal_name'] == $a){
            $filteredFeedingTimes = $v;
        }
    }

    $html .= '<div class="feeding_times">';
        if($filteredFeedingTimes['times']){
            $html .= '<i class="far fa-clock"></i>';
            $html .= '<ul>';
            foreach ($filteredFeedingTimes['times'] as $key => $value) {
                $html .= '<li>' . $value['time'] . '</li>';
            }
            $html .= '</ul>';
        }
    $html .= '</div>';

    return $html;

}

add_shortcode('animal-a-z', 'animalAZListings');
function animalAZListings()
{

    $component = file_get_contents(locate_template("partials/animal-a-z.php"));;
    // bit cheeky to load a vue component this way, but try and stop me
    $html = '';
    $html .= '<div id="app">';
    $html .= $component;
        $html .= '<animals-a-z></animals-a-z>';
    $html .= '</div>';
    return $html; // output
}
