<?php

class RestAPI {

    public function __construct()
    {
        add_action( 'rest_api_init', array($this, 'initRestAPI'));
    }

    public function initRestAPI()
    {
        register_rest_route( 'newquay/v2', '/get-animals', array(
            'methods' => 'GET',
            'callback' => array($this, 'getAnimals'),
            'permission_callback' => '__return_true'
        ));
    }

    public function getAnimals(WP_REST_Request $request)
    {
        $params = $request->get_params();

        $animals = get_posts(array(
            'post_type' => 'animals-az',
            'posts_per_page' => -1,
        ));

        foreach ($animals as $key => $value) {
            $letters = get_field('letters', $value->ID);
            $animals[$key]->featured_image = get_the_post_thumbnail($value->ID, 'large');
            $animals[$key]->letters = $letters;
            $animals[$key]->wp_link = get_the_permalink($value->ID);
        }

        return $animals;
    }

}
