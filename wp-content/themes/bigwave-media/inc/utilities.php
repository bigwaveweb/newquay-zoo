<?php
function fetchPagination($max)
{
    $big = 999999999; // need an unlikely integer
    $args = array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $max,
        'prev_text' => 'Prev Page',
        'next_text' => 'Next Page'
    );
    return paginate_links($args);
}

function returnColoursArray($i)
{
    $colours = [
        '#00b2d3',
        '#AD3A6D',
        '#9AB64B',
        '#004053'
    ];
    return $colours[$i];
}

function returnColoursArrayNoDarkBlue($i)
{
    $colours = [
        '#00b2d3',
        '#AD3A6D',
        '#9AB64B'
    ];
    return $colours[$i];
}


function returnExperiencesHTML($image, $colourIndex, $index)
{
    $colour = returnColoursArrayNoDarkBlue($colourIndex);
    $link = get_the_permalink();
    $title = get_the_title();

    return '
        <div class="col-12 col-md-6 item_column" style="background-color: '. $colour .';">
            <div class="accordion_item">
                <div class="accordion_toggle">
                    <button class="toggle_accordion_state">
                        <div class="svg_wrapper svg_top">
                            <svg preserveAspectRatio="none" style="isolation:isolate" viewBox="0 0 1569 34" version="1.1">
                                <g clip-path="url(#clipPath842)" id="g9" transform="translate(0,-0.5726952)">
                                    <path fill="#004053" id="path7" d="M 210.393,0 0,10.295 v 5.144 324.249 11.765 l 161.466,-6.62 104.381,5.15 192.46,-5.15 156.569,5.15 550.456,1.47 99.49,-11.765 304.178,5.145 V 5.145 L 1352.895,10.295 1268.898,8.09 1071.553,4.41 998.16,10.295 959.014,4.41 503.159,13.969 344.138,5.145 Z" />
                                </g>
                            </svg>
                        </div>
                        <div class="w-100 d-flex flex-row justify-content-between">
                            <h3>'.$title.'</h3>
                            <i class="fas fa-angle-right"></i>
                        </div>
                        <div class="svg_wrapper svg_bottom">
                            <svg preserveAspectRatio="none" style="isolation:isolate" viewBox="0 0 1569 34">
                                <g clip-path="url(#clipPath851)" id="g9" transform="rotate(180,784.5,17.04549)">
                                    <path fill="#004053" id="path7" d="M 210.393,0 0,10.295 v 5.144 324.249 11.765 l 161.466,-6.62 104.381,5.15 192.46,-5.15 156.569,5.15 550.456,1.47 99.49,-11.765 304.178,5.145 V 5.145 L 1352.895,10.295 1268.898,8.09 1071.553,4.41 998.16,10.295 959.014,4.41 503.159,13.969 344.138,5.145 Z" />
                                </g>
                            </svg>
                        </div>
                    </button>

                </div>

                <div class="accordion_content">
                    <a href="'.$link.'">
                        <div class="grid_item_image hidden_mob">'.$image.'</div>
                        <div class="accent">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 860 34.984">
                                <path class="a" style="fill: #004053;" d="M309.034,34.984H213.405l-70.014-1.158-31.63,1.158H88.45L0,33.826,0,4.295,143.391,1.266l76.4,3.029L328.937,2.15l67.7,1.07L427.963,2.6,558.249,0,703.5,4.295,768.857,2.15,860,4.295V32.521L719.648,34.178l-99.889-.352-61.509.352-105.616-.352-63.961-1.3Z"/>
                            </svg>
                            <h3 class="d-none d-md-block">'.$title.'</h3>
                        </div>
                    </a>

                </div>
                <div class="experiences_item_bottom d-none d-md-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 425 84.135">
                        <path fill="'.$colour.'" d="M16517.664-12293.865l-34.578-1.019-15.621,1.019h-11.514l-43.68-1.019v-14.262H16412v-54h.271v-11.1l70.814-2.653,37.73,2.653,53.9-1.876,33.438.934,15.469-.541,64.348-2.277,71.736,3.761,32.275-1.876,45.014,1.876v78.219l-69.316,1.449-49.332-.312-30.377.313-52.16-.312-31.59-1.137-39.332,2.155Z" transform="translate(-16412 12378)"/>
                    </svg>
                </div>
            </div>
        </div>
    ';
}



function returnListingsHTML($image, $flip, $colourIndex, $extraContent)
{

    $imageClass = 'order-1 order-md-1';
    $contentClass = 'order-2 order-md-2';

    if($flip){
        $imageClass = 'order-1 order-md-2';
        $contentClass = 'order-2 order-md-1';
    }

    $colour = returnColoursArray($colourIndex);

    $link = get_the_permalink();

    $string = '';
    if($extraContent){
        $string = '<div class="mt-3">' . $extraContent . '</div>';
    }

    return '
    <div class="row no-gutters">
        <div class="col-12 col-md-6 col-xl-7 '.$imageClass.'">
            <div class="grid_item_image">
                <a href="'.$link.'">
                    <img class="" src="'.$image.'" />
                </a>
            </div>
        </div>
        <div class="accent order-2">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 860 34.984">
                <path class="a" style="fill:'.$colour.';" d="M309.034,34.984H213.405l-70.014-1.158-31.63,1.158H88.45L0,33.826,0,4.295,143.391,1.266l76.4,3.029L328.937,2.15l67.7,1.07L427.963,2.6,558.249,0,703.5,4.295,768.857,2.15,860,4.295V32.521L719.648,34.178l-99.889-.352-61.509.352-105.616-.352-63.961-1.3Z"/>
            </svg>
        </div>
        <div class="col-12 col-md-6 col-xl-5 '.$contentClass.'">
            <div class="grid_item_content">
                <a href="'.$link.'"><h3>'. get_the_title(). '</h3></a>
                <div class="item_content">
                    '.$string.'
                    <a class="item_link" style="background-color: '.$colour.';" href="'.$link.'">Find out more</a>
                </div>
            </div>
        </div>
    </div>
    ';
}



// https://wphelp.blog/how-to-fix-error-404-in-pagination-with-wordpress-custom-permalinks/
function wphelp_custom_pre_get_posts( $query ) {
if( $query->is_main_query() && !$query->is_feed() && !is_admin() && is_category()) {
    $query->set( 'paged', str_replace( '/', '', get_query_var( 'page' ) ) );  }  }

add_action('pre_get_posts','wphelp_custom_pre_get_posts');

// https://wphelp.blog/how-to-fix-error-404-in-pagination-with-wordpress-custom-permalinks/
function wphelp_custom_request($query_string ) {
     if( isset( $query_string['page'] ) ) {
         if( ''!=$query_string['page'] ) {
             if( isset( $query_string['name'] ) ) { unset( $query_string['name'] ); } } } return $query_string; }

add_filter('request', 'wphelp_custom_request');
