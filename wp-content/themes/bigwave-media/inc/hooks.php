<?php

add_filter( 'wpex_localize_array', function( $array ) {
	unset( $array['menuWidgetAccordion'] );
	return $array;
} );

function myprefix_pagination_args( $args ) {
	$args['prev_text'] = 'Prev Page';
	$args['next_text'] = 'Next Page';
    $args['type'] = 'plain';
    $args['before_page_number'] = '';
	return $args;
}
add_filter( 'wpex_pagination_args', 'myprefix_pagination_args' );

add_action('wpex_hook_main_before', 'iconsNavWidget');
function iconsNavWidget()
{
    ?>
    <div class="icons_widget">
        <div class="icons_wrapper">
            <div class="blue">
                <a target="_blank" href="https://buy.myonlinebooking.co.uk/newquayzoo/sessions.aspx"><i class="fas fa-ticket-alt"></i></a>
                <a href="/newsletter-signup/"><i class="far fa-envelope"></i></a>
                <a target="_blank" href="https://shop.wildplanettrust.org.uk"><i class="fas fa-shopping-cart"></i></a>
            </div>
            <a href="/support-us/make-a-donation/" class="green d-none d-lg-block">Donate</a>
        </div>
    </div>
    <?php
}

function filter_next_post_sort($sort) {
    $sort = "ORDER BY p.post_title ASC LIMIT 1";
    return $sort;
}
function filter_next_post_where($where) {
    global $post, $wpdb;
    return $wpdb->prepare("WHERE p.post_title > '%s' AND p.post_type = '". get_post_type($post)."' AND p.post_status = 'publish'",$post->post_title);
}

function filter_previous_post_sort($sort) {
    $sort = "ORDER BY p.post_title DESC LIMIT 1";
    return $sort;
}
function filter_previous_post_where($where) {
    global $post, $wpdb;
    return $wpdb->prepare("WHERE p.post_title < '%s' AND p.post_type = '". get_post_type($post)."' AND p.post_status = 'publish'",$post->post_title);
}


add_action('wpex_hook_content_after', 'singlePostNavigation');
function singlePostNavigation()
{
    global $post;
    if($post && $post->post_type == 'animals-az') {
		add_filter('get_next_post_sort',   'filter_next_post_sort');
		add_filter('get_next_post_where',  'filter_next_post_where');

		add_filter('get_previous_post_sort',  'filter_previous_post_sort');
		add_filter('get_previous_post_where', 'filter_previous_post_where');
        ?>
		<div class="d-flex flex-column flex-md-row animals_nav justify-content-between">
            <div class="prev text-left">
                <?php echo previous_post_link('%link', '<span class="fa-stack fa-2x">
                    <i class="fas fa-circle fa-stack-1x"></i>
                    <i class="fas fa-angle-left fa-stack-1x fa-inverse"></i>
                </span> <div>%title</div>'); ?>
            </div>
            <div class="next text-right">
                <?php echo next_post_link('%link', '<div>%title</div> <span class="fa-stack fa-2x">
                    <i class="fas fa-circle fa-stack-1x"></i>
                    <i class="fas fa-angle-right fa-stack-1x fa-inverse"></i>
                </span>'); ?>
            </div>
        </div>
        <?php
    }
}

add_action('wpex_hook_sidebar_top', 'sidebarHideButtonMobile');
function sidebarHideButtonMobile()
{
    echo '<button class="d-block d-lg-none close_sidebar"><i class="fas fa-times"></i></button>';
}

add_action('wpex_hook_sidebar_before', 'sidebarToggleMobile');
function sidebarToggleMobile()
{
	$class = "";
	if($GLOBALS['hidden_header_section']){
		$class = "no_header_section";
	}
	echo '<button class="sidebar_toggle ' . $class . '">
        <img src="/wp-content/themes/bigwave-media/assets/sidebar_menu_toggle.svg" />
        <span>LINKS</span>
    </button>';
}

add_action('wpex_hook_main_before', 'featuredImageEmbed');
function featuredImageEmbed()
{

    global $post;

	// do nothing
	if(!$post || is_front_page() || $post->post_type == 'animal-experiences') {
		$GLOBALS['hidden_header_section'] = true;
		return;
	}

	// some defaults
    $backgroundColour = '#AD3A6D';
    $colour = '#ffffff';
	$img = get_the_post_thumbnail($post, 'full', array('class' => 'featured_bg'));
    $hide = get_field('hide_banner_breadcrumbs_section');
    $title = get_the_title();

    if ( !is_front_page() && is_home() ) {
        $page_id = get_option('page_for_posts');
        $img = get_the_post_thumbnail_url($page_id, 'full');
        $title = get_the_title($page_id);
    }

	if($hide || !$img || $post->post_type == 'animals-az' || $post->post_type == 'post' || is_search() ){
		$GLOBALS['hidden_header_section'] = true;
        echo '<div style="height: 60px;"></div>';
        return;
    }

    $breadcrumbs_background_colour = get_field('breadcrumbs_background_colour');
    if($breadcrumbs_background_colour){
        $backgroundColour = $breadcrumbs_background_colour;
    }
    $breadcrumbs_text_colour = get_field('breadcrumbs_text_colour');
    ?>

    <div class="featured_image">
        <?php echo $img; ?>
    </div>

    <div class="page_heading">
        <div class="svg_wrapper svg_top">
            <svg preserveAspectRatio="none" style="isolation:isolate" viewBox="0 0 1569 34" version="1.1">
                <g clip-path="url(#clipPath842)" id="g9" transform="translate(0,-0.5726952)"><path fill="<?php echo $backgroundColour; ?>" id="path7" d="M 210.393,0 0,10.295 v 5.144 324.249 11.765 l 161.466,-6.62 104.381,5.15 192.46,-5.15 156.569,5.15 550.456,1.47 99.49,-11.765 304.178,5.145 V 5.145 L 1352.895,10.295 1268.898,8.09 1071.553,4.41 998.16,10.295 959.014,4.41 503.159,13.969 344.138,5.145 Z" /></g>
            </svg>
        </div>
        <div style="background-color: <?php echo $backgroundColour; ?>">
            <div class="container">
                <div class="page_heading_inner">
                    <div class="page_heading_left d-none d-lg-flex align-items-center">
						<?php echo do_shortcode('[social_share]'); ?>
                    </div>
                    <div class="page_heading_right">
                        <h1 class="<?php echo strlen($title) > 15 ? 'shrink' : false; ?>"><?php echo $title; ?></h1>
                        <div class="<?php echo $breadcrumbs_text_colour ? $breadcrumbs_text_colour : 'white'; ?>_breadcrumbs bc_wrapper">
                            <?php wpex_get_template_part( 'breadcrumbs' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg_wrapper svg_bottom">
            <svg preserveAspectRatio="none" style="isolation:isolate" viewBox="0 0 1569 34">
                <g clip-path="url(#clipPath851)" id="g9" transform="rotate(180,784.5,17.04549)"><path fill="<?php echo $backgroundColour; ?>" id="path7" d="M 210.393,0 0,10.295 v 5.144 324.249 11.765 l 161.466,-6.62 104.381,5.15 192.46,-5.15 156.569,5.15 550.456,1.47 99.49,-11.765 304.178,5.145 V 5.145 L 1352.895,10.295 1268.898,8.09 1071.553,4.41 998.16,10.295 959.014,4.41 503.159,13.969 344.138,5.145 Z" /></g>
            </svg>
        </div>
    </div>
    <?php

}

add_action('bwm_social_share', 'bwmSocialShare');
function bwmSocialShare()
{
	echo getSocialShareHTML();
}

// dont remove
function postTypeSidebarDisplay( $sidebar ) {
	// Return a different sidebar for custom post type 'gallery'
	if ( is_singular( 'animals-az' ) ) {
		return 'a-zofanimalssidebar';
	}

	if( is_singular('childrens-clubs') ){
		return 'childrensclubssidebar';
	}

	if( is_singular('events') ){
		return 'eventssidebar';
	}

	if( is_singular('adopt-an-animal') ){
		return 'adoptananimalsidebar';
	}

	if( is_singular('animal-experiences') ){
		return 'animalexperiencessidebar';
	}
	// Return theme defined sidebar area
	else {
		return $sidebar;
	}
}
add_filter( 'wpex_get_sidebar', 'postTypeSidebarDisplay' );
