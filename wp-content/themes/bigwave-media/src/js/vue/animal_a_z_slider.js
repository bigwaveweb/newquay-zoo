
Vue.component('animals-a-z-slider', {

    template: '#animalsAZSlider',

    name: 'animalsAZSlider',

    // grabbing the two below via a cdn
    components: {
        'carousel': VueCarousel.Carousel,
        'slide': VueCarousel.Slide
    },

    data: function() {
        return {
            alphabet: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
        }
    },

    computed: {

    },

    methods: {

    },

    mounted: function() {

    },

});
