var element =  document.getElementById('app');

if(typeof(element) != 'undefined' && element != null){
    Vue.component('animals-a-z', {

        template: '#animalsAZ',

        name: 'animalsAZ',

        // grabbing the two below via a cdn
        components: {
            'carousel': VueCarousel.Carousel,
            'slide': VueCarousel.Slide
        },

        data: function() {
            return {
                animals: [],
                loadedSuccess: false,
                loaded: false,
                currentLetter: 'a',
                colours: ['#AD3A6D', '#00b2d3', '#9AB64B', '#004053'],
                alphabet: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
            }
        },

        computed: {
            filteredAnimals: function(){
                var self = this;
                return this.animals.filter(function(obj){
                    if(obj.letters){
                        for (var i = 0; i < obj.letters.length; i++) {
                            if(obj.letters[i].letter == self.currentLetter){
                                return true;
                            }
                        }
                    }
                    return obj.post_title.toLowerCase().startsWith(self.currentLetter);
                }).sort(function(a, b){
                    if(a.post_title < b.post_title) { return -1; }
                    if(a.post_title > b.post_title) { return 1; }
                    return 0;
                });
            },
        },

        methods: {
            getColourClass: function(index){
                var i = index %= 4;
                return this.colours[i];
            }
        },

        mounted: function() {

            var letterArr = new RegExp('[\?&]letter=([^&#]*)').exec(window.location.href);
            if(letterArr != null){
                var letter = decodeURI(letterArr[1]);
                if(letter){
                    this.currentLetter = letter;
                }
            }

            var self = this;
            // Make a request for a user with a given ID
            axios.get('/wp-json/newquay/v2/get-animals').then(function (response) {
                self.animals = response.data;
                self.loadedSuccess = true;
            })
            .catch(function (error) {
                console.log(error);
            })
            .then(function () {
                var _this = self;
                setTimeout(function(){
                    _this.loaded = true;
                }, 500);
            });
        },

    });
}
