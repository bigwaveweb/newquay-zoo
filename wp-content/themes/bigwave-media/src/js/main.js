import $ from 'jquery';
import 'bootstrap';
import './vue/animal_a_z.js';
import './vue/animal_a_z_slider.js';
import './app.js';

$(document).ready(() => {

    $('.icons_widget').fadeIn();

    $('.sidebar_toggle').click(function(){
        $('aside#sidebar').addClass('active');
    });

    $('.close_sidebar').click(function(){
        $('aside#sidebar').removeClass('active');
    });

    $('.open_menu_mobile').click(function(){
        if($('.nav_container:visible').length == 0){
            $('.nav_container').slideDown();
            $(this).find('.open_mobile_menu_bg').hide();
            $(this).find('i').removeClass('fa-bars').addClass('fa-times');
        } else {
            $('.nav_container').slideUp();
            $(this).find('.open_mobile_menu_bg').show();
            $(this).find('i').removeClass('fa-times').addClass('fa-bars');
        }
    });

    // desktop
    if($(window).width() > 991){

        $('#wrap, header, footer').hover(function(){
            $('li.menu-item-has-children').removeClass('open');
            $('.desktop_submenu_open_indictator').hide();
        });

        $('.nav_container ul.menu > li > div > a').hover(function() {
            // reset
            $('li.menu-item-has-children').removeClass('open');
            $('.desktop_submenu_open_indictator').hide();

            $(this).parent('div').parent('li').addClass('open');
            $(this).siblings('.desktop_submenu_open_indictator').show();

        });
    }

    $('.close_submenu').click(function(){
        $('.desktop_submenu_open_indictator').hide();
        $('li.menu-item-has-children').removeClass('open');
    });

    // mobile
    $('.nav_container ul.menu > li > div > i.open_mobile_submenu').click(function(){
        $(this).parent('div').parent('li').siblings().children('.sub_menu_wrapper').slideUp(600, 'linear');
        $(this).parent('div').parent('li').siblings().find('i.open_mobile_submenu').removeClass('fa-caret-down').addClass('fa-caret-right');
        if($(this).parent('div').siblings('.sub_menu_wrapper').is(':visible')){
            $(this).removeClass('fa-caret-down').addClass('fa-caret-right');
        } else {
            $(this).removeClass('fa-caret-right').addClass('fa-caret-down');
        }
        $(this).parent('div').siblings('.sub_menu_wrapper').slideToggle(600, 'linear');
    });

    $('.wpcf7 form input, .wpcf7 form select, .wpcf7 form textarea, form.newsletter_signup input').blur(function(){
        if ($(this).is(':radio') || $(this).is(':checkbox')) {
            return;
        }
        if(!$(this).val()){
            $(this).removeClass("filled");
            $(this).css('background-image', 'url(/wp-content/themes/bigwave-media/assets/form_input_bg_new.svg)');
        } else {
            $(this).addClass("filled");
            $(this).css('background-image', 'url(/wp-content/themes/bigwave-media/assets/form_input_submit_new.svg)');
        }
    });

    $('form.newsletter_signup input').each(function(index, obj){
        if( $(obj).val() ) {
            $(obj).addClass("filled");
            $(obj).css('background-image', 'url(/wp-content/themes/bigwave-media/assets/form_input_submit_new.svg)');
        }
    });

});
