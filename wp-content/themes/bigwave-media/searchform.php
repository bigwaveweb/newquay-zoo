<?php
/**
* The template for displaying search forms
*
* @package Total WordPress theme
* @subpackage Partials
* @version 5.0
*/

defined( 'ABSPATH' ) || exit;

$action = apply_filters( 'wpex_search_action', esc_url( home_url( '/' ) ), 'main' ); ?>

<form method="get" class="searchform" action="<?php echo esc_attr( $action ); ?>"<?php wpex_aria_landmark( 'searchform' ); ?>>
	<label>
		<input type="search" class="field" name="s" />
	</label>
	<?php if ( defined( 'ICL_LANGUAGE_CODE' ) ) : ?>
		<input type="hidden" name="lang" value="<?php echo( ICL_LANGUAGE_CODE ); ?>"/>
	<?php endif; ?>
	<?php do_action( 'wpex_searchform_fields' ); ?>

	<button type="submit" class="submit searchform-submit">
		<i class="fas fa-search"></i>
	</button>

</form>
