<?php
/**
* The template for displaying all pages, single posts and attachments
*
* @package Total WordPress Theme
* @subpackage Templates
* @version 5.0.6
*/

defined( 'ABSPATH' ) || exit;

get_header();

?>

<div id="content-wrap" class="container wpex-clr">

	<?php wpex_hook_primary_before(); ?>

	<div id="primary" class="content-area wpex-clr mt-5 mt-lg-0">

		<?php wpex_hook_content_before(); ?>

		<div id="content" class="site-content wpex-clr">

			<?php
			wpex_hook_content_top();
			$title = get_the_title();
			get_template_part('partials/animal-a-z-slider');
			?>

			<div class="container p-0">

				<div class="d-flex flex-column flex-md-row align-items-center justify-content-between">
					<div id="app" class="slider_container">
						<animals-a-z-slider></animals-a-z-slider>
					</div>
					<div class="d-none d-md-block a_z_breadcrumbs text-lg-right">
						<?php wpex_get_template_part( 'breadcrumbs' ); ?>
					</div>
				</div>

				<hr class="wp-block-separator has-text-color has-background has-blue-background-color has-blue-color is-style-dots">

				<div class="d-flex flex-column flex-md-row align-items-center justify-content-between">
					<div class="order-2 order-md-1">
						<?php
						echo '<h1>'.$title.'</h1>';
						$sciName = get_field('scientific_name');
						echo $sciName ? '<h3 class="fontPNCBR"><i>'.$sciName.'</i></h3>' : '';
						?>
					</div>
					<div class="mb-2 mb-md-0 a_z_back_to order-1 order-md-2">
						<a href="/explore/animals/?letter=<?php echo strtolower(substr($title, 0, 1)); ?>">
							<span class="bg" style="background-image: url(/wp-content/themes/bigwave-media/assets/pagination.svg);">
								<i class="fas fa-angle-left"></i>
							</span>
							<span class="text">Back to letter <?php echo substr($title, 0, 1); ?></span>
						</a>
					</div>
				</div>

				<?php
				// Display singular content unless there is a custom template defined
				if ( ! wpex_theme_do_location( 'single' ) ) :

					// Start loop
					while ( have_posts() ) : the_post();

					// Single Page
					if ( is_singular( 'page' ) ) {

						wpex_get_template_part( 'page_single_blocks' );

					}

					// Single posts
					elseif ( is_singular( 'post' ) ) {

						wpex_get_template_part( 'blog_single_blocks' );

					}

					// Portfolio Posts
					elseif ( is_singular( 'portfolio' ) && wpex_is_total_portfolio_enabled() ) {

						wpex_get_template_part( 'portfolio_single_blocks' );

					}

					// Staff Posts
					elseif ( is_singular( 'staff' ) && wpex_is_total_staff_enabled() ) {

						wpex_get_template_part( 'staff_single_blocks' );

					}

					// Testimonials Posts
					elseif ( is_singular( 'testimonials' ) && wpex_is_total_testimonials_enabled() ) {

						wpex_get_template_part( 'testimonials_single_blocks' );

					}

					/**
					* All other post types.
					*
					* When customizing your custom post types it's best to create
					* a new singular-{post_type}.php file to prevent any possible conflicts in the future
					* rather then altering the template part or create a dynamic template.
					*
					* @link https://wpexplorer-themes.com/total/docs/custom-post-type-singular-template/
					*/
					else {

						// Prevent issues with custom types named the same as core partial files.
						// @todo remove the $post_type paramater from wpex_get_template_part.
						$post_type = get_post_type();

						if ( in_array( $post_type, array( 'audio', 'video', 'gallery', 'content', 'comments', 'media', 'meta', 'related', 'share', 'title' ) ) ) {
							$post_type = null;
						}

						wpex_get_template_part( 'cpt_single_blocks', $post_type );

					}

					endwhile; ?>

				<?php endif; ?>

			</div>

			<?php wpex_hook_content_bottom(); ?>

		</div>

		<?php wpex_hook_content_after(); ?>

	</div>

	<?php wpex_hook_primary_after(); ?>

</div>

<?php get_footer(); ?>
