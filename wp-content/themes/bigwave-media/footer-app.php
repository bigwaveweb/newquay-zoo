<?php
/**
* The template for displaying the footer.
*
* @package Total WordPress Theme
* @subpackage Templates
* @version 5.0
*/

defined( 'ABSPATH' ) || exit;

?>
		</main>
	</div>
</div>


<?php wp_footer(); ?>

</body>
</html>
