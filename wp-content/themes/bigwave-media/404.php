<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Total WordPress Theme
 * @subpackage Templates
 * @version 5.0
 */

defined( 'ABSPATH' ) || exit;

get_header(); ?>

	<div id="content-wrap" class="wpex-clr p-0">

		<?php wpex_hook_primary_before(); ?>

		<div id="primary" class="content-area wpex-clr pb-0">

			<?php wpex_hook_content_before(); ?>

			<main id="content" class="site-content wpex-clr">

				<?php wpex_hook_content_top(); ?>

				<?php if ( ! wpex_theme_do_location( 'single' ) ) : ?>

					<article class="entry wpex-clr">

						<div class="error404-content wpex-text-center wpex-py-30 wpex-clr" style="background-image: url('/wp-content/themes/bigwave-media/assets/404.jpg');">

							<div class="error_404_overlay"></div>

							<div class="error_404_content container">
								<h1 class="error404-content-heading wpex-m-0 wpex-mb-10 wpex-text-6xl">404 Not Found!</h1>
								<h2 class="error404-content-heading wpex-m-0 wpex-mb-10 wpex-text-6xl"><?php esc_html_e( 'Sorry, this page could not be found.', 'total' ); ?></h2>
								<div class="error404-content-text wpex-text-md wpex-last-mb-0">
									<?php esc_html_e( 'The page you are looking for doesn\'t exist, no longer exists or has been moved.', 'total' ); ?>
								</div>

								<div class="row mt-3">
									<div class="col-12 col-lg-8 offset-lg-2">
										<div class="jagged_buttons">
											<div class="d-flex flex-column flex-sm-row justify-content-center jagged_buttons_inner">
												<div class="jagged_button_item">
													<a href="/">
														<span style="color: #ffffff; ?>;">Back to Homepage</span>
														<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 406.046 84.249">
															<g transform="translate(-27 -503.171)">
																<rect fill="#AD3A6D" class="a" width="406.046" height="75.673" transform="translate(27 508)"/>
																<path fill="#AD3A6D" class="a" d="M145.883,15.078H100.739l-33.05-.5-14.931.5h-11L0,14.579,0,1.851,67.689.545l36.066,1.306L155.278.927l31.96.461,14.786-.269L263.527,0l68.568,1.851L362.946.927l43.025.924V14.017l-66.254.714-47.153-.152-29.036.152-49.857-.152-30.193-.561Z" transform="translate(27 572.342)"/>
																<path fill="#AD3A6D" class="a" d="M145.883,15.078H100.739l-33.05-.5-14.931.5h-11L0,14.579,0,1.851,67.689.545l36.066,1.306L155.278.927l31.96.461,14.786-.269L263.527,0l68.568,1.851L362.946.927l43.025.924V14.017l-66.254.714-47.153-.152-29.036.152-49.857-.152-30.193-.561Z" transform="translate(27 503.171)"/>
															</g>
														</svg>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>

					</article>

				<?php endif; ?>

				<?php wpex_hook_content_bottom(); ?>

			</main>

			<?php wpex_hook_content_after(); ?>

		</div>

		<?php wpex_hook_primary_after(); ?>

	</div>

<?php get_footer(); ?>
