<?php
/**
* The template for displaying the footer.
*
* @package Total WordPress Theme
* @subpackage Templates
* @version 5.0
*/

defined( 'ABSPATH' ) || exit;

$img = apply_filters( 'wpex_header_logo_img_url', wpex_get_translated_theme_mod( 'custom_logo' ) );
$bwmCreditString = 'Website by <a target="_blank" href="https://bigwave.co.uk">Bigwave Marketing</a>';
?>
		</main>
	</div>
</div>

<footer class="site_footer">
	<img class="footer_top" src="/wp-content/themes/bigwave-media/assets/blue_horizontal_strip_top_rotate.svg" />
	<div class="footer_inner">
		<div class="container-fluid">
			<div class="row">
				<div class="d-none d-md-flex col-md-2 col-xl-2">
					<a href="/">
						<img src="<?php echo wpex_get_image_url( $img ); ?>" />
					</a>
				</div>
				<div class="d-none d-md-block col-12 col-md-5 col-lg-5 col-xl-6 px-md-0 px-xl-4">
					<?php echo get_template_part('partials/newsletter_signup'); ?>
					<?php wpex_footer_bottom_copyright(); ?>
				</div>
				<div class="col-12 col-md-5 col-lg-5 col-xl-4">
					<div class="row no-gutters">
						<div class="order-2 order-md-1 col-12 col-md-6 col-xl-7">
							<?php echo get_template_part('partials/other_zoos'); ?>
						</div>
						<div class="order-1 order-md-2 col-12 col-md-6 col-xl-5">
							<?php echo get_template_part('partials/social'); ?>
							<div class="d-none d-md-block bwm_credit">
								<?php echo $bwmCreditString; ?>
							</div>
						</div>
						<div class="order-3 col-12 d-block d-md-none">
							<?php echo get_template_part('partials/newsletter_signup'); ?>
						</div>
					</div>
					<div class="mobile_copyright d-block d-md-none">
						&copy; Newquay Zoo <?php echo date('Y'); ?> <br />
						Registered charity number 300923
					</div>
					<?php wpex_footer_bottom_menu(); ?>
					<div class="d-block d-md-none bwm_credit">
						<?php echo $bwmCreditString; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
