<?php
/**
* The header for our theme.
*
* Displays all of the <head> section and everything up till <div id="main">
*
* @package Total WordPress Theme
* @subpackage Templates
* @version 5.0
*/

defined( 'ABSPATH' ) || exit;

?><!doctype html>
<html <?php language_attributes(); ?><?php wpex_schema_markup( 'html' ); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php wp_body_open(); ?>

	<div id="outer-wrap" class="wpex-clr">

		<header class="main_site_header">
			<div class="header_wrapper">
				<div class="container-fluid container-xl">
					<div class="row justify-content-between align-items-center align-items-lg-end">
						<?php wpex_header_logo(); ?>
						<div class="header_right">

							<button class="open_menu_mobile">
								<div class="open_menu_mobile_inner">
									<i class="fa fa-bars"></i>
									<img class="open_mobile_menu_bg" src="/wp-content/themes/bigwave-media/assets/menu_widget_closed_new.svg" />
									<img src="/wp-content/themes/bigwave-media/assets/menu_widget_open_new.svg" />
								</div>
							</button>

							<div class="header_row top">
								<div class="top_bar">
									<?php echo get_template_part('partials/social'); ?>
									<div class="site_top_bar">
										<?php wpex_top_bar(); ?>
									</div>
								</div>
							</div>

							<div class="header_row">
								<div class="row_item_links_wrapper">
									<div class="row_item">
										<a target="_blank" href="https://buy.myonlinebooking.co.uk/newquayzoo/sessions.aspx">
											<i class="fas fa-ticket-alt"></i>
											<span>Buy<br />Tickets</span>
										</a>
									</div>
									<div class="row_item">
										<a target="_blank" href="https://shop.wildplanettrust.org.uk">
											<i class="fas fa-shopping-cart"></i>
											<span>Online<br />Shop</span>
										</a>
									</div>
								</div>
								<a target="_blank" class="other_sites" href="https://www.paigntonzoo.org.uk/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/paignton-zoo-logo.png" /></a>
								<a target="_blank" class="other_sites" href="https://www.wildplanettrust.org.uk/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/wildplanettrust-logo.png" /></a>
							</div>
							<div class="header_row">
								<div class="searchbar">
									<?php get_search_form(); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<img class="header_bottom" src="/wp-content/themes/bigwave-media/assets/blue_horizontal_strip_bottom.svg" />
		</header>

		<div class="nav_container">
			<nav>
				<div class="container-fluid container-lg px-0 px-lg-3">
					<?php
					wp_nav_menu(array(
						'theme_location' => 'main_menu',
						'walker' => new BWM_Navwalker()
					));
					?>
				</div>
				<img class="nav_bottom d-block d-lg-none" src="/wp-content/themes/bigwave-media/assets/green_horizontal_strip_top.svg" />
			</nav>
			<img class="nav_bottom d-none d-lg-block" src="/wp-content/themes/bigwave-media/assets/green_horizontal_strip_top.svg" />
		</div>

		<div id="wrap" class="wpex-clr">

			<?php wpex_hook_main_before(); ?>

			<main id="main" class="site-main wpex-clr"<?php wpex_schema_markup( 'main' ); ?><?php wpex_aria_landmark( 'main' ); ?>>
