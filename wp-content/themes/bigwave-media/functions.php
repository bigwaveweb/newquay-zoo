<?php
/**
* Child theme functions
*
* When using a child theme (see http://codex.wordpress.org/Theme_Development
* and http://codex.wordpress.org/Child_Themes), you can override certain
* functions (those wrapped in a function_exists() call) by defining them first
* in your child theme's functions.php file. The child theme's functions.php
* file is included before the parent theme's file, so the child theme
* functions would be used.
*
* Text Domain: wpex
* @link http://codex.wordpress.org/Plugin_API
*
*/
if(!is_admin()){
	$inc = array(
		'/utilities.php',
		'/navwalker.php',
		'/shortcodes.php',
		'/rest_api.php',
		'/rss.php',
		'/hooks.php',
	);
	foreach ( $inc as $file ) {
		require_once 'wp-content/themes/bigwave-media/inc' . $file;
	}
}

include get_stylesheet_directory() . '/cards/init.php';


add_action('admin_head', 'animalExperiencesBG');
function animalExperiencesBG()
{
	global $post;
	$background_colour = get_field('background_colour');
	if($post && is_admin() && $post->post_type == 'animal-experiences' && $background_colour){
		list($r, $g, $b) = sscanf($background_colour, "#%02x%02x%02x");
		echo "<style> .editor-styles-wrapper { background-color: rgba(".$r.", ".$g.", ".$b.", 0.5); } </style>";
	}
}

/**
* Load the parent style.css file
*
* @link http://codex.wordpress.org/Child_Themes
*/
function total_child_enqueue_parent_theme_style()
{

	// Dynamically get version number of the parent stylesheet (lets browsers re-cache your stylesheet when you update your theme)
	$theme   = wp_get_theme( 'Total' );
	$version = $theme->get( 'Version' );

	// Load the stylesheet
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array(), $version );

}
add_action( 'wp_enqueue_scripts', 'total_child_enqueue_parent_theme_style' );


function themeEnqueueStyles()
{
	wp_enqueue_script( 'fa-kit', 'https://kit.fontawesome.com/32a3ee021d.js', array('jquery') );

	wp_enqueue_script( 'carousel', 'https://cdn.rawgit.com/SSENSE/vue-carousel/6823411d/dist/vue-carousel.min.js', array('jquery'), true, true );
	wp_enqueue_script( 'vue-js', 'https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.min.js', array('jquery'), true, true );
	wp_enqueue_script( 'vue-axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js', array('vue-js'), true, true );

	// wp_enqueue_script( 'simplebar_js', 'https://unpkg.com/simplebar@latest/dist/simplebar.min.js', array('jquery'), true, true );
	// wp_enqueue_style( 'simplebar_css', 'https://unpkg.com/simplebar@latest/dist/simplebar.css', array('wpex-style') );

	wp_enqueue_style( 'theme-style', get_stylesheet_directory_uri() . '/dist/css/style.min.css', array('wpex-style') );
	wp_enqueue_script( 'theme-script', get_stylesheet_directory_uri() . '/dist/js/main.min.js', array('jquery'), true, true );
	// bit overkill but this styling should be first, but only peasants use bog standard css
}
add_action( 'wp_enqueue_scripts', 'themeEnqueueStyles' );

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
	'page_title' 	=> 'Newquay Zoo General Settings',
	'menu_title'	=> 'Newquay Zoo Settings',
	'menu_slug' 	=> 'nzoo-general-settings',
	'capability'	=> 'edit_posts',
	'redirect'		=> false
	));
}

// remove_action( 'wp_head', 'wp_generator');
// remove_action( 'wp_head', 'wlwmanifest_link' );
// remove_action( 'comments_atom_head', 'the_generator' );
// remove_action( 'wp_head', 'rsd_link' );
// remove_action( 'wp_head', 'wlwmanifest_link' );
// remove_action( 'commentsrss2_head', 'the_generator' );
// remove_action( 'wp_head', 'feed_links_extra', 3);
// remove_action( 'wp_head', 'feed_links', 2);
// remove_action( 'rss2_head', 'the_generator' );
// remove_action( 'rss_head',  'the_generator' );
// remove_action( 'atom_head', 'the_generator' );
// remove_action( 'opml_head', 'the_generator' );
// add_filter( 'xmlrpc_enabled', '__return_false' );

// remove versioning from scripts
add_filter( 'the_generator', '__return_empty_string' );
function remove_version_scripts_styles( $src )
{
	if ( strpos( $src, 'ver=' ) )
	{
		$src = remove_query_arg( 'ver', $src );
	}
	return $src;
}
add_filter( 'style_loader_src', 'remove_version_scripts_styles', 9999 );
add_filter( 'script_loader_src', 'remove_version_scripts_styles', 9999 );

if(!is_admin()){
	new RestAPI();
}

add_action('template_redirect', 'my_custom_disable_author_page');

function my_custom_disable_author_page() {
    global $wp_query;

    if ( is_author() ) {
        // Redirect to homepage, set status to 301 permenant redirect.
        // Function defaults to 302 temporary redirect.
        wp_redirect(get_option('home'), 301);
        exit;
    }
}
