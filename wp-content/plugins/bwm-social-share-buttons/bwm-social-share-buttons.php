<?php
/**
 * Plugin Name: BWM Social Share Buttons
 * Plugin URI: https://bigwave.co.uk
 * Description: Social Share Buttons
 * Version: 0.1
 * Author: Tudor
 */

 // Add Shortcode
function fk_addtoany( $atts )
{

    //current url
    global $wp;

    $url = home_url( $wp->request );

    $object = get_queried_object();

    // Attributes
    $atts = shortcode_atts(array(
        'facebook' => 'true',
        'twitter' => 'true',
        'title' => get_bloginfo('name') . ' - ' . $object->post_title,
    ), $atts );

    $title = get_the_title($post);

    if($atts['title']){
        $title = $atts['title'];
    }

    $colourString = 'blue_bg';
    if(is_page()){
        $colour = get_field('social_share_colour', $object->ID);
        $colourString = $colour ? $colour . '_bg' : '';
    }

    $html = '';

    $html .= '<div class="social_share '.$colourString.'"><span class="share_heading">Share:</span>';
        if($atts['facebook']){
            $html .= '<a class="social_share_button fa fa-facebook" href="https://www.facebook.com/sharer/sharer.php?u=' . $url .'&display=page" title="Twitter" rel="nofollow noopener" target="_blank"></a>';
        }
        if($atts['twitter']){
            $html .= '<a class="social_share_button fa fa-twitter" href="https://twitter.com/intent/tweet?url=' . $url . '&text=' . $title.'" title="Facebook" rel="nofollow noopener" target="_blank"></a>';
        }
    $html .= '</div>';

    return $html;

}
add_shortcode( 'social_share', 'fk_addtoany' );
