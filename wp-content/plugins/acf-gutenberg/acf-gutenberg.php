<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://bigwavemedia.co.uk
 * @since             1.0.0
 * @package           Acf_Gutenberg
 *
 * @wordpress-plugin
 * Plugin Name:       ACF Gutenberg Blocks
 * Plugin URI:        https://bigwavemedia.co.uk
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Daniel Sheen
 * Author URI:        https://bigwavemedia.co.uk
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       acf-gutenberg
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'ACF_GUTENBERG_VERSION', '1.0.0' );


function acfTdaGutenbergInit()
{
	acf_update_setting('google_api_key', 'AIzaSyB5CSXhKYo71YxDxSFDhNfMVvBthxs_Whc');
}
add_action('acf/init', 'acfTdaGutenbergInit');


function activate_acf_gutenberg()
{
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-acf-gutenberg-activator.php';
	Acf_Gutenberg_Activator::activate();
}

function deactivate_acf_gutenberg()
{
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-acf-gutenberg-deactivator.php';
	Acf_Gutenberg_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_acf_gutenberg' );
register_deactivation_hook( __FILE__, 'deactivate_acf_gutenberg' );

require plugin_dir_path( __FILE__ ) . 'includes/class-acf-gutenberg.php';


function run_acf_gutenberg()
{
	$plugin = new Acf_Gutenberg();
	$plugin->run();
}
run_acf_gutenberg();
