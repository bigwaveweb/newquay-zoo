if(!$) {
    var $ = jQuery;
}

function initMap( $el ) {
    var $markers = $el.find('.marker');
    var mapArgs = {
        zoom        : $el.data('zoom') || 16,
        mapTypeId   : google.maps.MapTypeId.ROADMAP,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.RIGHT_CENTER,
        },
        zoomControl: true,
        zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_CENTER,
        },
        streetViewControl: false,
    };
    var map = new google.maps.Map( $el[0], mapArgs );
    map.markers = [];
    $markers.each(function(){
        initMarker( $(this), map );
    });
    centerMap( map );
    return map;
}

function initMarker( $marker, map ) {
    var lat = $marker.data('lat');
    var lng = $marker.data('lng');
    var latLng = {
        lat: parseFloat( lat ),
        lng: parseFloat( lng )
    };
    var marker = new google.maps.Marker({
        position : latLng,
        map: map,
        icon: '/wp-content/plugins/acf-gutenberg/public/assets/map_marker.png'
    });
    map.markers.push( marker );
    if( $marker.html() ){
        var infowindow = new google.maps.InfoWindow({
            content: $marker.html()
        });
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open( map, marker );
        });
    }
}

function centerMap( map ) {
    var bounds = new google.maps.LatLngBounds();
    map.markers.forEach(function( marker ){
        bounds.extend({
            lat: marker.position.lat(),
            lng: marker.position.lng()
        });
    });
    if( map.markers.length == 1 ){
        map.setCenter( bounds.getCenter() );
    } else{
        map.fitBounds( bounds );
    }
}

$(document).ready(function(){
    $('.acf_map').each(function(){
        var map = initMap( $(this) );
    });

    // accordion
    $('.toggle_accordion_state').click(function(){
        $(this).parent().toggleClass('active');
        $(this).parent().siblings('.accordion_content').find('.grid_item_image').slideToggle();
        $(this).parent().siblings('.accordion_content').find('.item_content').slideToggle();
        $(this).parent().siblings('.accordion_content').find('.final_accordion_accent_bottom svg').toggle();
    });

    if($('.conservation_status').length > 0){
        var lv = $('.conservation_notch').data('conservation-value');


        switch (true) {
            case (lv >= 85):
                $('.conservation_notch span').html('EW');
                $('.conservation_status_name').html('EXTINCT IN THE WILD');
                var colour = '#250816';

            break;
            case (lv >= 65):
                $('.conservation_notch span').html('CR');
                $('.conservation_status_name').html('CRITICALLY ENDANGERED');
                var colour = '#911806';

            break;
            case (lv >= 50):
                $('.conservation_notch span').html('EN');
                $('.conservation_status_name').html('ENDANGERED');
                var colour = '#e02f21';

            break;
            case (lv >= 35):
                $('.conservation_notch span').html('VU');
                $('.conservation_status_name').html('VULNERABLE');
                var colour = '#ee8b23';

            break;
            case (lv >= 15):
                $('.conservation_notch span').html('NT');
                $('.conservation_status_name').html('NEAR THREATENED');
                var colour = '#85c63f';

            break;
            default:
                $('.conservation_notch span').html('LC');
                $('.conservation_status_name').html('LEAST CONCERN');
                var colour = '#108254';

        }

        $('.conservation_status_name').css('color', colour);

        $('.conservation_notch')
        .css('transform', 'translateX(-' + lv + '%)')
        .animate({
            'left': lv + '%',
            'opacity': 1,
        }, 500);
    }

});

var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {

    var playerDiv = $('.player_container');
    var videoID = $(playerDiv).data('yt-id');
    var loop = $(playerDiv).data('loop');
    var autoplay = $(playerDiv).data('autoplay');

    var autoplayResult = 0;
    if(autoplay){
        setTimeout(function(){
            $('.video_overlay').fadeOut();
        }, 1000);
        autoplayResult = 1;
    }

    player = new YT.Player('player', {
        videoId: videoID,
        playerVars: { 'autoplay': autoplayResult},
        events: {
            'onReady': function(event){

                var playButton = $('.play_video');
                if(autoplay == 'true'){
                    setTimeout(function(){
                        $('.video_overlay').fadeOut();
                        event.target.playVideo();
                    }, 1000);
                }

                $(playButton).click(function() {
                    $('.video_overlay').fadeOut();
                    event.target.playVideo();
                });

            },
            'onStateChange': function(event){
                if(event.target.getPlayerState() == 2){
                    event.target.pauseVideo();
                    $('.video_overlay').fadeIn();
                } else if (event.target.getPlayerState() == 0) {
                    if(loop == 'true'){
                        event.target.playVideo();
                    } else {
                        $('.video_overlay').fadeIn();
                    }
                }
            }
        }
    });
}
