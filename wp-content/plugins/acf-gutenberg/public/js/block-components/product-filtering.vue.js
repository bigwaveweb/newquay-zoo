
Vue.component('product-filtering', {

    template: '#productFiltering',

    props: ['category'],

    name: 'productFiltering',

    data: function() {
        return {
            productCats: [],
            subCats: [],
            activeCat: '',
            activeSubCat: '',
            showProducts: [],
            showSubCatButtons: false
        }
    },

    watch: {

        showProducts: {
            handler: function(oldVal, newVal) {

                for (var i = 0; i < newVal.length; i++) {
                    var product_attributes = newVal[i].attributes;
                    var product_variations = newVal[i].variations;

                    if( product_attributes && product_variations ){
                        var attribute_names = {};
                        for (var x = 0; x < product_attributes.length; x++) {
                            var attribute_name = 'attribute_' + product_attributes[x].attr_name;
                            attribute_names[attribute_name] = product_attributes[x].selected;
                        }

                        var result = product_variations.filter(function(obj, index) {
                            var variation_attributes = obj.attributes;
                            var filterResult = false;
                            if( JSON.stringify(variation_attributes) == JSON.stringify(attribute_names) ) {
                                filterResult = true;
                            }
                            return filterResult;
                        });

                        if ( result.length > 0 ) {
                            this.showProducts[i].add_to_cart_link = result[0].variation_id;
                        } else {
                            this.showProducts[i].add_to_cart_link = false;
                        }

                    } else {
                        this.showProducts[i].add_to_cart_link = false;
                    }
                }
            }, deep: true
        },

        activeCat: function() {
            if(this.activeSubCat){
                this.activeSubCat = '';
            }
            this.filterProducts();
        },
        activeSubCat: function() {
            this.filterProducts();
        },
        showSubCatButtons: function(){
            if(!this.showSubCatButtons){
                this.activeSubCat = '';
            }
        }
    },

    methods: {

        capitalize: function (value) {
            if (!value) return ''
            value = value.toString()
            return value.charAt(0).toUpperCase() + value.slice(1)
        },

        groupBy: function(xs, key) {
            return xs.reduce(function(rv, x) {
                (rv[x[key]] = rv[x[key]] || []).push(x);
                return rv;
            }, {});
        },

        filterProducts: function(){
            var self = this;

            this.showProducts = [];

            axios.post('/wp-json/newquay_zoo/v2/run-products-filter', {
                product_cat: self.activeCat,
                sub_cat: self.activeSubCat,
            }).then(function (response) {
                if(response.data.products){
                    self.showProducts = response.data.products;
                    self.showSubCatButtons = true;
                } else if (response.data.categories) {
                    self.subCats = response.data.categories;
                    self.showSubCatButtons = true;
                } else {
                    self.showSubCatButtons = false;
                }

                if(!response.data.selected_child && !response.data.categories) {
                    self.showSubCatButtons = false;
                }
            }).catch(function (error) {
                console.log(error);
            });

        },

        setActiveCat: function(id){
            if(this.activeCat && this.activeCat == id){
                this.activeCat = '';
            } else if (this.activeCat) {
                this.activeCat = id;
            } else {
                this.activeCat = id;
            }
        },

        setActiveSubCat: function(id){
            if(this.activeSubCat && this.activeSubCat == id){
                this.activeSubCat = '';
            } else if (this.activeSubCat) {
                this.activeSubCat = id;
            } else {
                this.activeSubCat = id;
            }
        },

    },

    mounted: function () {
        this.$nextTick(function () {

            var self = this;

            axios.post('/wp-json/newquay_zoo/v2/get-product-cats', {
                parent_cat: self.category,
            }).then(function (response) {
                self.productCats = response.data.product_cat;
            }).catch(function (error) {
                console.log(error);
            });

        })
    },

});
