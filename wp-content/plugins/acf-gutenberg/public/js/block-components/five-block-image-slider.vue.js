
Vue.component('five-block-slider', {

    template: '#fiveBlockSlider',

    props: ['block-id', 'post-id'],

    name: 'fiveBlockSlider',

    data: function() {
        return {
            slideData: [],
            activeSlide: 0,
            lightboxImage: false,
            lightboxCaption: false,
        }
    },

    methods: {

        getBgImage: function(slide){
            if(slide.slide_override_image){
                return 'background-image: url(' + slide.slide_override_image.url + ');';
            } else {
                return 'background-image: url(' + slide.featured_image + ');';
            }
        },

        createLightbox: function(imageData, image, isFeatured, caption){

            this.lightboxCaption = caption;

            var imageArray = [];

            if(isFeatured){
                imageArray.push(image);
                for (var i = 0; i < imageData.length; i++) {
                    imageArray[i + 1] = imageData[i].image.url;
                }
            } else {
                for (var i = 0; i < imageData.length; i++) {
                    imageArray[i] = imageData[i].image.url;
                }
            }

            this.lightboxImage = imageArray;

        },

        slideNext: function(){
            var slideCount = this.slideData.length;

            var currentSlide = this.activeSlide + 1;

            if(currentSlide == slideCount){
                this.activeSlide = 0;
            } else {
                this.activeSlide++;
            }
        },

        slidePrev: function(){
            var slideCount = this.slideData.length;

            var currentSlide = this.activeSlide;

            if(currentSlide == 0){
                this.activeSlide = slideCount - 1;
            } else {
                this.activeSlide--;
            }
        },

    },

    mounted: function () {
        this.$nextTick(function () {

            var self = this;

            axios.post('/wp-json/newquay_zoo/v2/get-acf-data', {
                post_id: self.postId,
                block_id: self.blockId
            }).then(function (response) {
                self.slideData = response.data.slides;
            }).catch(function (error) {
                console.log(error);
            });

        })
    },

});
