
Vue.component('vertical-slider', {

    template: '#verticalSlider',

    props: ['block-id', 'post-id'],

    name: 'verticalSlider',

    data: function() {
        return {
            slideData: [],
            activeSlide: 0,
            lightboxImage: false,
            lightboxCaption: false,
            margin: [],
            padding: []
        }
    },

    methods: {

        buildMargin: function(){
            return this.margin.top + 'px ' + this.margin.right + 'px ' + this.margin.bottom + 'px ' + this.margin.left + 'px ';
        },

        buildPadding: function(){
            return this.padding.top + 'px ' + this.padding.right + 'px ' + this.padding.bottom + 'px ' + this.padding.left + 'px ';
        },

        slideNext: function(){
            var slideCount = this.slideData.length;

            var currentSlide = this.activeSlide + 1;

            if(currentSlide == slideCount){
                this.activeSlide = 0;
            } else {
                this.activeSlide++;
            }
        },

        slidePrev: function(){
            var slideCount = this.slideData.length;

            var currentSlide = this.activeSlide;

            if(currentSlide == 0){
                this.activeSlide = slideCount - 1;
            } else {
                this.activeSlide--;
            }
        },

    },

    mounted: function () {
        this.$nextTick(function () {

            var self = this;
            axios.post('/wp-json/newquay_zoo/v2/get-acf-data', {
                post_id: self.postId,
                block_id: self.blockId
            }).then(function (response) {
                self.slideData = response.data.slides;
                self.margin = response.data.margin;
                self.padding = response.data.padding;
            }).catch(function (error) {
                console.log(error);
            });

        })
    },

});
