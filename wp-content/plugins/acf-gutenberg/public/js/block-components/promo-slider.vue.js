
Vue.component('promo-slider', {

    template: '#promoSlider',

    props: ['block-id', 'post-id'],

    name: 'promoSlider',

    data: function() {
        return {
            slideData: [],
            activeSlide: 0,
            loading: true,
            cutout: 'white',
            backgroundColour: 'blue'
        }
    },

    methods: {

        getBgImage: function(image){
            if(!image.url){
                return image.source_url;
            }
            return image.url;
        },


        slideNext: function(){
            var slideCount = this.slideData.length;

            var currentSlide = this.activeSlide + 1;

            if(currentSlide == slideCount){
                this.activeSlide = 0;
            } else {
                this.activeSlide++;
            }
        },

        slidePrev: function(){
            var slideCount = this.slideData.length;

            var currentSlide = this.activeSlide;

            if(currentSlide == 0){
                this.activeSlide = slideCount - 1;
            } else {
                this.activeSlide--;
            }
        },

    },

    mounted: function () {
        this.$nextTick(function () {

            var self = this;

            axios.post('/wp-json/newquay_zoo/v2/get-acf-data', {
                post_id: self.postId,
                block_id: self.blockId
            }).then(function (response) {
                self.style = response.data.style;
                self.slideData = response.data.slides;
                self.loading = false;
                self.cutout = response.data.cutout_colour;
                self.backgroundColour = response.data.background_colour;
            }).catch(function (error) {
                console.log(error);
            });

        })
    },

});
