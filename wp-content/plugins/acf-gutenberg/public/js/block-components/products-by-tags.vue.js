
Vue.component('products-by-tags', {

    template: '#productByTags',

    props: ['tag'],

    name: 'productByTags',

    data: function() {
        return {
            showProducts: [],
        }
    },

    methods: {

        capitalize: function (value) {
            if (!value) return ''
            value = value.toString()
            return value.charAt(0).toUpperCase() + value.slice(1)
        },

    },

    mounted: function () {
        this.$nextTick(function () {

            var self = this;

            axios.post('/wp-json/newquay_zoo/v2/get-products-by-tag', {
                tag: self.tag,
            }).then(function (response) {
                self.showProducts = response.data;
            }).catch(function (error) {
                console.log(error);
            });

        })
    },

});
