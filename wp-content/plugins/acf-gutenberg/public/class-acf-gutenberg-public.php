<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://bigwavemedia.co.uk
 * @since      1.0.0
 *
 * @package    Acf_Gutenberg
 * @subpackage Acf_Gutenberg/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Acf_Gutenberg
 * @subpackage Acf_Gutenberg/public
 * @author     Daniel Sheen <danielsheen@bigwavemedia.co.uk>
 */
class Acf_Gutenberg_Public
{

	private $plugin_name;

	private $version;

	public function __construct( $plugin_name, $version )
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		add_action( 'rest_api_init', array($this, 'getAcfFields'));
		add_filter('get_the_excerpt',  array($this, 'change_excerpt') );

	}

	function change_excerpt($text)
	{
		return rtrim($text,'[...]');
	}

	public function getAcfFields()
	{
		register_rest_route( 'newquay_zoo/v2', '/get-acf-data', array(
			'methods' => 'POST',
			'callback' => array($this, 'getAcfData'),
			'permission_callback' => '__return_true'
		));
	}

	function getAcfData(WP_REST_Request $request)
	{
		$returnBlock = '';
		$blockData = array();

		$params = $request->get_params();
		$postID = $params['post_id'];
		$blockID = $params['block_id'];

		$pid = get_post($postID);
		$blocks = parse_blocks( $pid->post_content );
		$collect = array();

		foreach($blocks as $block){
			if(isset($block['attrs']['id']) && $blockID == $block['attrs']['id']){
				acf_setup_meta( $block['attrs']['data'], $block['attrs']['id'], true );
				$fields = get_fields();
				$collect[$block['attrs']['id']] = $fields;
				acf_reset_meta( $block['attrs']['id'] );
			} else {
				continue;
			}
		}

		$response = $collect[$blockID];
		if(isset($response['slides'])){
			foreach ($response['slides'] as $key => $value) {
				if($value['slide_post']){
					$featuredImageUrl = get_the_post_thumbnail_url($value['slide_post']->ID, 'large');
					$response['slides'][$key]['featured_image'] = $featuredImageUrl;
					$response['slides'][$key]['link'] = get_the_permalink($value['slide_post']);
				}
			}
		}

		return $response;
	}

	public function enqueue_styles()
	{
		wp_enqueue_style( 'slick-css', plugin_dir_url( __FILE__ ) . 'css/slick.css' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/acf-gutenberg-public.css', array(), $this->version, 'all' );
	}

	public function enqueue_scripts()
	{
		// wp_enqueue_script( 'slick-js', plugin_dir_url( __FILE__ ) . 'js/slick.min.js', array('jquery'), $this->version, false);
		// wp_enqueue_script( 'font-awesome', 'https://kit.fontawesome.com/a08a13ecd8.js', array('jquery'), $this->version, false);

		// wp_enqueue_script( 'vue-js', plugin_dir_url( __FILE__ ) . 'js/vue.min.js', array('jquery'), $this->version, true, true);

		wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB5CSXhKYo71YxDxSFDhNfMVvBthxs_Whc', array('jquery'), true, true);
		// wp_enqueue_script( 'vue-js', 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.11/vue.js', array('jquery'), true, true);
		// wp_enqueue_script( 'vue-axios', plugin_dir_url( __FILE__ ) . 'js/axios.min.js', array('vue-js'), $this->version, false );

		// wp_enqueue_script( 'promo-slider-block', plugin_dir_url( __FILE__ ) . 'js/block-components/promo-slider.vue.js', array( 'vue-axios' ), $this->version, false );
		// wp_enqueue_script( 'product-filtering-vue', plugin_dir_url( __FILE__ ) . 'js/block-components/product-filtering.vue.js', array( 'vue-axios' ), $this->version, false );
		// wp_enqueue_script( 'products-by-tags-vue', plugin_dir_url( __FILE__ ) . 'js/block-components/products-by-tags.vue.js', array( 'vue-axios' ), $this->version, false );
		// wp_enqueue_script( 'vertical-slider-vue', plugin_dir_url( __FILE__ ) . 'js/block-components/vertical-slider.vue.js', array( 'vue-axios' ), $this->version, false );
		// wp_enqueue_script( 'five-block-slider-vue', plugin_dir_url( __FILE__ ) . 'js/block-components/five-block-image-slider.vue.js', array( 'vue-axios' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/main.js', array( 'vue-axios' ), $this->version, false );
	}

}
