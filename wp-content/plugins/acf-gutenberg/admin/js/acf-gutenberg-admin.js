(function( $ ) {
	acf.add_filter('color_picker_args', function( args, $field ){
		args.palettes = [
			'#004053',
			'#00b2d3',
			'#AD3A6D',
			'#9AB64B',
			'#691339',
			'#ffffff',
			'#000000'
		]
		return args;
	});
})( jQuery );

wp.domReady(function(){
	wp.blocks.registerBlockStyle( 'core/image', {
		name: 'jagged',
		label: 'Jagged',
		isDefault: false,
	});

	wp.blocks.registerBlockStyle( 'core/table', {
		name: 'animal-experiences',
		label: 'Animal Experiences',
		isDefault: false,
	});
});
