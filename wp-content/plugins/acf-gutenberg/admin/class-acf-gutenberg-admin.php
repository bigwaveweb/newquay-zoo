<?php

/**
* The admin-specific functionality of the plugin.
*
* @link       https://bigwavemedia.co.uk
* @since      1.0.0
*
* @package    Acf_Gutenberg
* @subpackage Acf_Gutenberg/admin
*/

/**
* The admin-specific functionality of the plugin.
*
* Defines the plugin name, version, and two examples hooks for how to
* enqueue the admin-specific stylesheet and JavaScript.
*
* @package    Acf_Gutenberg
* @subpackage Acf_Gutenberg/admin
* @author     Daniel Sheen <danielsheen@bigwavemedia.co.uk>
*/
class Acf_Gutenberg_Admin
{

	private $plugin_name;

	private $version;

	public function __construct( $plugin_name, $version )
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		add_filter( 'block_categories', array($this, 'gutenbergBlocksCategory'), 10, 2);
		add_action( 'after_setup_theme', array($this, 'colourSetup') );
		add_action( 'acf/init', array($this, 'gutenbergInit') );

	}


	public function colourSetup()
	{
		// Disable Custom Colors
		add_theme_support( 'disable-custom-colors' );

		// Editor Color Palette
		add_theme_support( 'editor-color-palette', array(
			array(
			'name'  => __( 'Blue', 'newquay Zoo' ),
			'slug'  => 'blue',
			'color'	=> '#004053',
			),
			array(
			'name'  => __( 'Light Blue', 'newquay Zoo' ),
			'slug'  => 'light_blue',
			'color' => '#00b2d3',
			),
			array(
			'name'  => __( 'Pink', 'newquay Zoo' ),
			'slug'  => 'pink',
			'color' => '#AD3A6D',
			),
			array(
			'name'  => __( 'Dark Pink', 'newquay Zoo' ),
			'slug'  => 'dark-pink',
			'color' => '#691339',
			),
			array(
			'name'	=> __( 'Green', 'newquay Zoo' ),
			'slug'	=> 'green',
			'color'	=> '#9AB64B',
			),
			array(
			'name'	=> __( 'White', 'newquay Zoo' ),
			'slug'	=> 'white',
			'color'	=> '#ffffff',
			),
			array(
			'name'	=> __( 'Black', 'newquay Zoo' ),
			'slug'	=> 'white',
			'color'	=> '#000000',
			),
			) );
		}

		public function gutenbergBlocksCategory( $categories, $post )
		{
			return array_merge($categories, array(
			array(
			'slug' => 'newquay_zoo',
			'title' => __( 'newquay Zoo', 'newquay Zoo' )
			)
			));
		}

		public function gutenbergBlockCallback( $block )
		{
			$slug = str_replace('acf/', '', $block['name']);
			if( file_exists("wp-content/plugins/acf-gutenberg/blocks/".$slug.".php" ) ) {
				include("wp-content/plugins/acf-gutenberg/blocks/".$slug.".php");
			}
		}

		public function gutenbergBlockData($callbackStr)
		{
			$blocks = array(
			array(
				'name'				=> 'text-image-block',
				'title'				=> __('Text Image Block (grid)'),
				'description'		=> __('Text Image Block, in grid layout'),
				'render_callback'	=> $callbackStr,
				'category'			=> 'newquay_zoo',
				'icon'				=> 'slides',
				'mode' 				=> 'edit',
				'supports'			=> array('mode' => false),
				'keywords'			=> array( 'image', 'text', 'grid', 'colour', 'link' ),
			),
			array(
				'name'				=> 'text-image-alternate-block',
				'title'				=> __('Text Image Block (Alternate styling)'),
				'description'		=> __('Text Image Block, alternate styling'),
				'render_callback'	=> $callbackStr,
				'category'			=> 'newquay_zoo',
				'icon'				=> 'slides',
				'mode' 				=> 'edit',
				'supports'			=> array('mode' => false),
				'keywords'			=> array( 'image', 'text', 'grid', 'colour', 'link' ),
			),
			array(
				'name'				=> 'conservation-status',
				'title'				=> __('Conservation Status Block'),
				'description'		=> __('Conservation Status Block'),
				'render_callback'	=> $callbackStr,
				'category'			=> 'newquay_zoo',
				'icon'				=> 'slides',
				'mode' 				=> 'edit',
				'supports'			=> array('mode' => false),
				'keywords'			=> array( 'conservation', 'status' ),
			),
			array(
				'name'				=> 'animal-details',
				'title'				=> __('Animal Details'),
				'description'		=> __('Animal Details'),
				'render_callback'	=> $callbackStr,
				'category'			=> 'newquay_zoo',
				'icon'				=> 'slides',
				'mode' 				=> 'edit',
				'supports'			=> array('mode' => false),
				'keywords'			=> array( 'conservation', 'status' ),
			),
			array(
				'name'				=> 'jagged-buttons',
				'title'				=> __('Jagged Buttons Block'),
				'description'		=> __('Jagged Buttons Block'),
				'render_callback'	=> $callbackStr,
				'category'			=> 'newquay_zoo',
				'icon'				=> 'slides',
				'mode' 				=> 'edit',
				'supports'			=> array('mode' => false),
				'keywords'			=> array( 'jagged', 'buttons', 'cta' ),
			),
			array(
				'name'				=> 'responsive-spacer',
				'title'				=> __('Responsive Spacer'),
				'description'		=> __('Responsive Spacer Block'),
				'render_callback'	=> $callbackStr,
				'category'			=> 'newquay_zoo',
				'icon'				=> 'slides',
				'mode' 				=> 'edit',
				'supports'			=> array('mode' => false),
				'keywords'			=> array( 'spacer', 'responsive' ),
			),
			array(
				'name'				=> 'flexible-content',
				'title'				=> __('Flexible Content'),
				'description'		=> __('Flexible Content Block'),
				'render_callback'	=> $callbackStr,
				'category'			=> 'newquay_zoo',
				'icon'				=> 'editor-table',
				'mode' 				=> 'edit',
				'supports'			=> array('mode' => false),
				'keywords'			=> array( 'tabs', 'content', 'text', 'cta', 'image', 'link', 'switch' ),
			),
			array(
				'name'				=> 'accordion',
				'title'				=> __('Accordion Block'),
				'description'		=> __('Accordion Block'),
				'render_callback'	=> $callbackStr,
				'category'			=> 'newquay_zoo',
				'icon'				=> 'editor-table',
				'mode' 				=> 'edit',
				'supports'			=> array('mode' => false),
				'keywords'			=> array( 'accordion', 'content', 'text', 'image', 'link' ),
			)
		);

		return $blocks;

	}

	public function gutenbergInit()
	{
		$callback = array($this, 'gutenbergBlockCallback');
		$blocks = $this->gutenbergBlockData($callback);
		if($blocks){
			foreach ($blocks as $block) {
				if( function_exists('acf_register_block') ) {
					acf_register_block_type($block);
				}
			}
		}
	}

	public function enqueue_styles()
	{
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/acf-gutenberg-admin.css', array(), $this->version, 'all' );
	}

	public function enqueue_scripts()
	{

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/acf-gutenberg-admin.js', array( 'jquery' ), $this->version, false );

	}

}
