<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://bigwavemedia.co.uk
 * @since      1.0.0
 *
 * @package    Acf_Gutenberg
 * @subpackage Acf_Gutenberg/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Acf_Gutenberg
 * @subpackage Acf_Gutenberg/includes
 * @author     Daniel Sheen <danielsheen@bigwavemedia.co.uk>
 */
class Acf_Gutenberg_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
