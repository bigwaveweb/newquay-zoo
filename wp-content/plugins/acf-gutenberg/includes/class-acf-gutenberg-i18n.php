<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://bigwavemedia.co.uk
 * @since      1.0.0
 *
 * @package    Acf_Gutenberg
 * @subpackage Acf_Gutenberg/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Acf_Gutenberg
 * @subpackage Acf_Gutenberg/includes
 * @author     Daniel Sheen <danielsheen@bigwavemedia.co.uk>
 */
class Acf_Gutenberg_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'acf-gutenberg',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
