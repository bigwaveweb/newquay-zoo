<?php

/**
 * Fired during plugin activation
 *
 * @link       https://bigwavemedia.co.uk
 * @since      1.0.0
 *
 * @package    Acf_Gutenberg
 * @subpackage Acf_Gutenberg/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Acf_Gutenberg
 * @subpackage Acf_Gutenberg/includes
 * @author     Daniel Sheen <danielsheen@bigwavemedia.co.uk>
 */
class Acf_Gutenberg_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
