<?php
/**
* Block Name: Accordion
*
* This is the template that displays the Accordion block.
*/

$id = 'accordion-' . $block['id'];

$accordion = get_field('accordion');
$jagged = get_field('jagged');

$backgroundColour = get_field('background_colour');
if(!$backgroundColour){
    $backgroundColour = '#004053';
}
$textColour = get_field('text_colour');
if(!$textColour){
    $textColour = '#ffffff';
}
?>

<div class="bwm_accordion" id="<?php echo $id; ?>" <?php echo $margin; ?>>
    <div class="accordion" id="bwm_accordion_<?php echo $block['id']; ?>" <?php echo $padding; ?>>

        <?php
        foreach ($accordion as $key => $value) {
            $image = $value['image'];
            $buttonBackgroundColour = $value['button_background_colour'] ? $value['button_background_colour'] : '#ffffff';
            $buttonTextColour = $value['button_text_colour'] ? $value['button_text_colour'] : '#004053';
            ?>
            <div class="card <?php echo $jagged ? 'jagged' : 'not_jagged'; ?>">
                <?php if($jagged){ ?>
                    <div class="svg_wrapper svg_top">
                        <svg preserveAspectRatio="none" style="isolation:isolate" viewBox="0 0 1569 34" version="1.1">
                            <g clip-path="url(#clipPath842)" id="g9" transform="translate(0,-0.5726952)">
                                <path fill="<?php echo $backgroundColour; ?>" id="path7" d="M 210.393,0 0,10.295 v 5.144 324.249 11.765 l 161.466,-6.62 104.381,5.15 192.46,-5.15 156.569,5.15 550.456,1.47 99.49,-11.765 304.178,5.145 V 5.145 L 1352.895,10.295 1268.898,8.09 1071.553,4.41 998.16,10.295 959.014,4.41 503.159,13.969 344.138,5.145 Z" />
                            </g>
                        </svg>
                    </div>
                <?php } ?>
                <div class="card-header" style="background-color: <?php echo $backgroundColour; ?>;" id="heading_<?php echo $key; ?>">
                    <button style="color: <?php echo $textColour; ?>" class="collapsed d-flex justify-content-between btn btn-link accordion_open" type="button" data-toggle="collapse" data-target="#collapse_<?php echo $block['id']; ?>_<?php echo $key; ?>" aria-expanded="false" aria-controls="collapse_<?php echo $block['id']; ?>_<?php echo $key; ?>" >
                        <?php echo $value['accordion_dropdown_heading']; ?>
                        <i class="fas fa-angle-right"></i>
                    </button>
                </div>
                <div id="collapse_<?php echo $block['id']; ?>_<?php echo $key; ?>" class="collapse" aria-labelledby="heading_<?php echo $key; ?>" data-parent="#bwm_accordion_<?php echo $block['id']; ?>" >
                    <div class="card-body" style="color: <?php echo $textColour; ?>; background-color: <?php echo $backgroundColour; ?>;">
                        <div class="row">

                            <?php
                            echo $image ? '<div class="col-12 col-lg-6 my-auto">' : '<div class="col-12 my-auto">';
                            echo $value['item_heading'] ? '<h3 style="color: '.$textColour.';">'.$value['item_heading'].'</h3>' : '';

                            if($value['replace_content_with_shortcode']){
                                do_shortcode($value['replace_content_with_shortcode']);
                            } else {
                                echo $value['accordion_content'] ? $value['accordion_content'] : '';
                            }
                            echo $value['link'] ? '<a class="accordion_link" style="background-color: '.$buttonBackgroundColour.'; color: '.$buttonTextColour.';" href="'.$value['link']['url'].'" target="'.$value['link']['target'].'">'.$value['link']['title'].'</a>' : '';
                            echo '</div>';
                            echo $image ? '<div class="col-12 col-lg-6"><img alt="accordion image" src="'.$image['url'].'" /></div>' : '';
                            ?>

                        </div>
                    </div>
                </div>
                <?php if($jagged){ ?>
                    <div class="svg_wrapper svg_bottom">
                        <svg preserveAspectRatio="none" style="isolation:isolate" viewBox="0 0 1569 34">
                            <g clip-path="url(#clipPath851)" id="g9" transform="rotate(180,784.5,17.04549)">
                                <path fill="<?php echo $backgroundColour; ?>" id="path7" d="M 210.393,0 0,10.295 v 5.144 324.249 11.765 l 161.466,-6.62 104.381,5.15 192.46,-5.15 156.569,5.15 550.456,1.47 99.49,-11.765 304.178,5.145 V 5.145 L 1352.895,10.295 1268.898,8.09 1071.553,4.41 998.16,10.295 959.014,4.41 503.159,13.969 344.138,5.145 Z" />
                            </g>
                        </svg>
                    </div>
                <?php } ?>
            </div>
            <?php
        }
        ?>

    </div>
</div>
