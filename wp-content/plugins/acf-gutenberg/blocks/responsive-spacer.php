<?php
/**
* Block Name: Responsive Spacer
*
* This is the template that displays the Responsive Spacer block.
*/

$id = 'responsive-spacer-' . $block['id'];

$spacerSettings = get_field('spacer_settings');
// bit hacky and dirty, but what you gonna do about it :P
if(!is_admin()){
    ?>
    <style>
        #<?php echo $id; ?> {
            height: <?php echo $spacerSettings['mobile']; ?>px;
        }
        @media(min-width: 768px){
            #<?php echo $id; ?> {
                height: <?php echo $spacerSettings['tablet']; ?>px;
            }
        }
        @media(min-width: 1200px){
            #<?php echo $id; ?> {
                height: <?php echo $spacerSettings['desktop']; ?>px;
            }
        }
    </style>
    <div class="responsive_spacer" id="<?php echo $id; ?>"></div>
    <?php
}
