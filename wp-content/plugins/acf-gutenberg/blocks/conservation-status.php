<?php
/**
* Block Name: Conservation Status
*
* This is the template that displays the Conservation Status block.
*/

$id = 'conservation-status-' . $block['id'];

$conservation_status = get_field('conservation_status');
?>

<div class="conservation_status" id="<?php echo $id; ?>">
    <span class="conservation_status_name_wrapper">IUCN Conservation Status - <span class="conservation_status_name"></span></span>
    <div class="conservation_status_inner">
        <div class="conservation_bg"></div>
        <div class="conservation_notch" data-conservation-value="<?php echo $conservation_status; ?>">
            <span></span>
        </div>
        <span class="least_concern">
            <span>Least Concern</span>
            <div class="lc_line">
                <div></div>
                <div></div>
            </div>
        </span>
        <span class="extinct_wild">
            <div class="ew_line">
                <div></div>
                <div></div>
            </div>
            <span>Extinct In The Wild</span>
        </span>
    </div>
</div>
