<?php
/**
* Block Name: Jagged Buttons
*
* This is the template that displays the Jagged Buttons block.
*/

$id = 'jagged-buttons-' . $block['id'];

$conservation_status = get_field('conservation_status');
$buttons = get_field('buttons');
$bgcolour = get_field('background_colour');
$textColor = get_field('text_colour');
?>

<div class="jagged_buttons" id="<?php echo $id; ?>">
    <div class="d-flex flex-column flex-sm-row justify-content-center jagged_buttons_inner">

        <?php
        if ($buttons) {
            foreach ($buttons as $key => $button) {
                ?>
                <div class="jagged_button_item">
                    <a href="<?php echo $button['button']['url'] ?>" target="<?php echo $button['button']['target'] ?>">
                        <span style="color: <?php echo $textColor; ?>;"><?php echo $button['button']['title'] ?></span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 406.046 84.249">
                            <g transform="translate(-27 -503.171)">
                                <rect fill="<?php echo $bgcolour; ?>" class="a" width="406.046" height="75.673" transform="translate(27 508)"/>
                                <path fill="<?php echo $bgcolour; ?>" class="a" d="M145.883,15.078H100.739l-33.05-.5-14.931.5h-11L0,14.579,0,1.851,67.689.545l36.066,1.306L155.278.927l31.96.461,14.786-.269L263.527,0l68.568,1.851L362.946.927l43.025.924V14.017l-66.254.714-47.153-.152-29.036.152-49.857-.152-30.193-.561Z" transform="translate(27 572.342)"/>
                                <path fill="<?php echo $bgcolour; ?>" class="a" d="M145.883,15.078H100.739l-33.05-.5-14.931.5h-11L0,14.579,0,1.851,67.689.545l36.066,1.306L155.278.927l31.96.461,14.786-.269L263.527,0l68.568,1.851L362.946.927l43.025.924V14.017l-66.254.714-47.153-.152-29.036.152-49.857-.152-30.193-.561Z" transform="translate(27 503.171)"/>
                            </g>
                        </svg>
                    </a>
                </div>
                <?php
            }
        }
        ?>

    </div>
</div>
