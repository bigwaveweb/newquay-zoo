<?php
/**
* Block Name: Text Image Block (alternate)
*
* This is the template that displays the Text Image Block (alternate).
*/

$id = 'text-image-block' . $block['id'];

$grid_items = get_field('grid_items');

$textColour = get_field('text_colour');

if(!$textColour){
    $textColour = '#004053';
}
$bgColour = get_field('background_colour');
if(!$bgColour){
    $bgColour = $textColour;
}
$zIndex = get_field('z-index');


$accordion = get_field('accordion_on_mobile');

$flexClass = 'flex-md-column';
if(!$accordion){
    $display = get_field('content_display');
    $flexClass = $display == 'side_by_side' ? 'flex-md-row' : 'flex-md-column';
}

$alternatingLayout = get_field('alternating_layout');

if(!$alternatingLayout){
    $imageClass = 'order-md-1';
    $contentClass = 'order-md-2';
    if(get_field('image_leftright') == 'right'){
        $imageClass = 'order-2';
        $contentClass = 'order-1';
    }

}

$i = 0;
$len = count($grid_items);
$finalAccordionBottom = '';

?>

<div class="text_image_alternate_block <?php echo $alternatingLayout ? 'alternating_layout' : false ?> <?php echo $accordion ? 'accordion' : ''; ?>" id="<?php echo $id; ?>" style="z-index: <?php echo $zIndex ? $zIndex : '0'; ?>;">
    <?php
    if($grid_items){

        echo $accordion ? '<div class="text_image_grid"><div class="accordion_wrapper">' : '<div class="text_image_grid">';

        foreach ($grid_items as $key => $value) {
            $headingStyle='style="color: '.$textColour.';"';
            if($value['heading_colour']){
                $headingStyle='style="color: '.$value['heading_colour'].';"';
            }
            $colour = $value['accent_colour'] ? $value['accent_colour'] : $bgColour;
            $linkColour = $value['link_colour'] ? $value['link_colour'] : $bgColour;

            if($accordion && $i == $len - 1){
                $finalAccordionBottom = '<div class="final_accordion_accent_bottom d-block d-md-none"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 860 34.984">
                    <path class="a" style="fill:'.$colour .'" d="M309.034,34.984H213.405l-70.014-1.158-31.63,1.158H88.45L0,33.826,0,4.295,143.391,1.266l76.4,3.029L328.937,2.15l67.7,1.07L427.963,2.6,558.249,0,703.5,4.295,768.857,2.15,860,4.295V32.521L719.648,34.178l-99.889-.352-61.509.352-105.616-.352-63.961-1.3Z"/>
                </svg></div>';
            }

            if($alternatingLayout){
                $imageClass = 'order-2';
                $contentClass = 'order-1';
                if($accordion){
                    $imageClass = 'order-1 order-md-2';
                    $contentClass = 'order-2 order-md-1';
                }

                if($key % 2 == 0){
                    $imageClass = 'order-1 order-md-1';
                    $contentClass = 'order-2 order-md-2';
                }
            }

            $image = wp_get_attachment_image($value['image']['id'], 'large', false);

            if($accordion){
                ?>
                <div class="accordion_item">
                    <div class="accordion_toggle">
                        <button class="toggle_accordion_state">
                            <div class="svg_wrapper svg_top">
                                <svg preserveAspectRatio="none" style="isolation:isolate" viewBox="0 0 1569 34" version="1.1">
                                    <g clip-path="url(#clipPath842)" id="g9" transform="translate(0,-0.5726952)">
                                        <path fill="#004053" id="path7" d="M 210.393,0 0,10.295 v 5.144 324.249 11.765 l 161.466,-6.62 104.381,5.15 192.46,-5.15 156.569,5.15 550.456,1.47 99.49,-11.765 304.178,5.145 V 5.145 L 1352.895,10.295 1268.898,8.09 1071.553,4.41 998.16,10.295 959.014,4.41 503.159,13.969 344.138,5.145 Z" />
                                    </g>
                                </svg>
                            </div>
                            <div class="w-100 d-flex flex-row justify-content-between">
                                <?php echo $value['heading'] ? '<h3>' . $value['heading'] . '</h3>' : '<h3>Click here to toggle</h3>'; ?>
                                <i class="fas fa-angle-right"></i>
                            </div>
                            <div class="svg_wrapper svg_bottom">
                                <svg preserveAspectRatio="none" style="isolation:isolate" viewBox="0 0 1569 34">
                                    <g clip-path="url(#clipPath851)" id="g9" transform="rotate(180,784.5,17.04549)">
                                        <path fill="#004053" id="path7" d="M 210.393,0 0,10.295 v 5.144 324.249 11.765 l 161.466,-6.62 104.381,5.15 192.46,-5.15 156.569,5.15 550.456,1.47 99.49,-11.765 304.178,5.145 V 5.145 L 1352.895,10.295 1268.898,8.09 1071.553,4.41 998.16,10.295 959.014,4.41 503.159,13.969 344.138,5.145 Z" />
                                    </g>
                                </svg>
                            </div>
                        </button>
                    </div>
                    <div class="accordion_bg" style="background-color: <?php echo $colour; ?>;"></div>
                    <div class="accordion_content">
                <?php
            }
            ?>

            <div class="row no-gutters">
                <div class="col-12 col-md-6 <?php echo $imageClass; ?>">
                    <div class="grid_item_image <?php echo $accordion ? 'hidden_mob' : ''; ?>">
                        <?php echo $image; ?>
                    </div>
                </div>
                <div class="col-12 col-md-6 <?php echo $contentClass; ?>">
                    <div class="grid_item_content">
                        <?php
                        if($accordion){
                            echo '
                            <div class="svg_wrapper svg_top">
                                <svg preserveAspectRatio="none" style="isolation:isolate" viewBox="0 0 1569 34" version="1.1">
                                    <g clip-path="url(#clipPath842)" id="g9" transform="translate(0,-0.5726952)">
                                        <path fill="'.$colour.'" id="path7" d="M 210.393,0 0,10.295 v 5.144 324.249 11.765 l 161.466,-6.62 104.381,5.15 192.46,-5.15 156.569,5.15 550.456,1.47 99.49,-11.765 304.178,5.145 V 5.145 L 1352.895,10.295 1268.898,8.09 1071.553,4.41 998.16,10.295 959.014,4.41 503.159,13.969 344.138,5.145 Z" />
                                    </g>
                                </svg>
                            </div>
                            ';
                            echo $value['heading'] ? '<h3 class="d-none d-md-block" '.$headingStyle.'>' . $value['heading'] . '</h3>' : '';
                        } else {
                            echo $value['heading'] ? '<h3 '.$headingStyle.'>' . $value['heading'] . '</h3>' : '';
                        }
                        ?>

                        <div class="item_content <?php echo $accordion ? 'hidden_mob' : ''; ?>">
                            <div class="d-flex flex-column <?php echo $flexClass; ?>">
                                <div><?php echo $value['content'] ? $value['content'] : ''; ?></div>
                                <div><?php echo $value['additional_content'] ? $value['additional_content']: ''; ?></div>
                            </div>
                            <?php echo $value['link'] ? '<a class="item_link" style="background-color: '.$linkColour.';" href="'.$value['link']['url'].'" target="'.$value['link']['target'].'">'.$value['link']['title'].'</a>' : ''; ?>
                        </div>

                        <?php echo $finalAccordionBottom; ?>

                    </div>
                </div>

                <div class="accent <?php echo $accordion ? 'd-none d-md-block' : '' ?>">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 860 34.984">
                        <path class="a" style="fill:<?php echo $colour; ?>;" d="M309.034,34.984H213.405l-70.014-1.158-31.63,1.158H88.45L0,33.826,0,4.295,143.391,1.266l76.4,3.029L328.937,2.15l67.7,1.07L427.963,2.6,558.249,0,703.5,4.295,768.857,2.15,860,4.295V32.521L719.648,34.178l-99.889-.352-61.509.352-105.616-.352-63.961-1.3Z"/>
                    </svg>
                </div>

            </div>

            <?php
            echo $accordion ? '</div></div>' : '';
            $i++;
        }

        echo $accordion ? '</div></div>' : '</div>';
    }
    ?>
</div>
