<?php
/**
* Block Name: Animal Details
*
* This is the template that displays the Animal Details block.
*/

$id = 'animal-details-' . $block['id'];

$conservation_status = get_field('conservation_status');
$class = get_field('class');
$order = get_field('order');
$family = get_field('family');
?>

<div class="animal_details" id="<?php echo $id; ?>">
    <div class="d-flex flex-column flex-md-row justify-content-start animal_details_inner">
        <?php
        if($class){
            echo '<div><strong>Class: </strong><span>'.$class.'</span></div>';
        }
        if($order){
            echo '<div><strong>Order: </strong><span>'.$order.'</span></div>';
        }
        if($family){
            echo '<div><strong>Family: </strong><span>'.$family.'</span></div>';
        }
        ?>
    </div>
</div>
