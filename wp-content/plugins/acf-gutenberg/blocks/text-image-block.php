<?php
/**
* Block Name: Text Image Block (grid)
*
* This is the template that displays the Text Image Block (grid).
*/

$id = 'text-image-block' . $block['id'];

$grid_items = get_field('grid_items');
$zIndex = get_field('z-index');
$textColour = get_field('text_colour');
$bgColour = get_field('background_colour');


?>

<div class="text_image_block" id="<?php echo $id; ?>" style="z-index: <?php echo $zIndex; ?>;">
    <?php
    if($grid_items){
        echo '<div class="text_image_grid">';
        foreach ($grid_items as $key => $value) {
            $imageClass = 'order-2 order-md-1';
            $contentClass = 'order-1 order-md-2';
            if($value['image_leftright'] == 'right'){
                $imageClass = 'order-2';
                $contentClass = 'order-1';
            }

            $imageWrapperID = 'item_' . $block['id'] . '_' . $key;

            $headingStyle='style="color: '.$textColour.';"';
            if($value['heading_colour']){
                $headingStyle='style="color: '.$value['heading_colour'].';"';
            }

            $imageBG='style="background-color: '.$bgColour.';"';
            if($value['image_background']){
                $imageBG='style="background-color: '.$value['image_background'].';"';
            }

            $popClass = 'pop';
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false) {
                $popClass = '';
            }

            $image = wp_get_attachment_image($value['image']['id'], 'large', false, array(
                'style' => $value['position'] ? 'object-position:' . $value['position'] . ';' : false,
            ));

            ?>
            <div class="row no-gutters">
                <div class="col-12 col-md-6 <?php echo $imageClass; ?>">
                    <div id="<?php echo $imageWrapperID; ?>" class="grid_item_image <?php echo $value['image_pops_out'] ? $popClass : ''; ?>" <?php echo $imageBG; ?>>
                        <?php
                        if($value['image_pops_out']){
                            ?>
                            <img src="<?php echo $value['image']['sizes']['large'] ? $value['image']['sizes']['large'] : $value['image']['url']; ?>" style="<?php echo $value['position'] ? 'object-position:' . $value['position'] . ';' : false; ?>" class="<?php echo $value['image_pops_out'] ? $popClass : ''; ?>" />
                            <?php
                        } else {
                            echo $image;
                        }
                        ?>
                    </div>
                </div>
                <div class="col-12 col-md-6 <?php echo $contentClass; ?>">
                    <div class="grid_item_content <?php echo $value['image_pops_out'] ? 'pop' : ''; ?>">

                        <?php
                        if($value['link']){
                            echo $value['heading'] ? '
                                <a href="'.$value['link']['url'].'" target="'.$value['link']['target'].'">
                                    <h3 '.$headingStyle.'>' . $value['heading'] . '</h3>
                                    <span class="link_arrow fa-stack fa-1x">
                                        <i '.$headingStyle.' class="fas fa-circle fa-stack-1x"></i>
                                        <i class="fas fa-angle-right fa-stack-1x fa-inverse"></i>
                                    </span>
                                </a>
                            ' : '';
                        } else {
                            echo $value['heading'] ? '<h3 '.$headingStyle.'>' . $value['heading'] . '</h3>' : '';
                        }
                        ?>

                        <div class="item_content">
                            <?php echo $value['content'] ? $value['content'] : ''; ?>
                            <?php echo $value['link'] ? '<a class="item_link" '.$imageBG.' href="'.$value['link']['url'].'" target="'.$value['link']['target'].'">'.$value['link']['title'].'</a>' : ''; ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        echo '</div>';
    }
    ?>
</div>
