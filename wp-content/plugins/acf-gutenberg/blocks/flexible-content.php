<?php
/**
* Block Name: Flexible Content
*
* This is the template that displays the Flexible Content block.
*/

$id = 'flexible-content-' . $block['id'];

$content = get_field('content');
$heading = get_field('heading');
$headingLv = get_field('heading_level');
$buttons = get_field('buttons');
$textAlign = get_field('text_align');
$zIndex = get_field('z-index');
$jagged = get_field('jagged_topbottom');

$backgroundColour = get_field('background_colour');
$textColour = get_field('text_colour');
$buttonBG = get_field('button_background_colour');
$buttonText = get_field('button_colour');

$content_width = get_field('content_width');
if($content_width == 'full'){
    $widthClass = 'col-md-12';
} elseif ($content_width == 'large') {
    $widthClass = 'col-md-10 offset-md-1';
} else {
    $widthClass = 'col-md-8 offset-md-2';
}

$buttonStyle = 'style="background-color: '.$buttonBG.'; color: '.$buttonText.';"';
?>

<div class="flexible_content <?php echo $block['className']; ?> <?php echo $jagged == 'both' || $jagged == 'top' ? 'jagged_top' : ''; ?> <?php echo $jagged == 'both' || $jagged == 'bottom' ? 'jagged_bottom' : ''; ?>" id="<?php echo $id; ?>" style="z-index: <?php echo $zIndex; ?>;">
    <div class="content">
        <?php if($jagged == 'both' || $jagged == 'top'){ ?>
            <div class="svg_wrapper svg_top">
                <svg preserveAspectRatio="none" style="isolation:isolate" viewBox="0 0 1569 34" version="1.1">
                    <g clip-path="url(#clipPath842)" id="g9" transform="translate(0,-0.5726952)">
                        <path fill="<?php echo $backgroundColour; ?>" id="path7" d="M 210.393,0 0,10.295 v 5.144 324.249 11.765 l 161.466,-6.62 104.381,5.15 192.46,-5.15 156.569,5.15 550.456,1.47 99.49,-11.765 304.178,5.145 V 5.145 L 1352.895,10.295 1268.898,8.09 1071.553,4.41 998.16,10.295 959.014,4.41 503.159,13.969 344.138,5.145 Z" />
                    </g>
                </svg>
            </div>
        <?php } ?>

        <div class="content_inner" style="background-color: <?php echo $backgroundColour; ?>">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 <?php echo $widthClass; ?> no_img_flex_content_inner">
                        <?php
                        $colorStyle = $textColour ? $textColour : '#ffffff';
                        if($content || $heading) {
                            echo '<div class="main_content" style="color: '.$colorStyle.'; text-align: '.$textAlign.';">';
                                echo $heading ? '<'.$headingLv.' style="color: '.$colorStyle.';">' . $heading . '</'.$headingLv.'>' : '';
                                echo $content ? $content : '';
                            echo '</div>';
                        }
                        if($buttons){
                            echo '<div class="buttons">';
                            foreach ($buttons as $button) {
                                echo '<div><a href="'.$button['button']['url'].'" target="'.$button['button']['target'].'" '.$buttonStyle.'>
                                '.$button['button']['title'].'
                                </a></div>';
                            }
                            echo '</div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <?php if($jagged == 'both' || $jagged == 'bottom'){ ?>
            <div class="svg_wrapper svg_bottom">
                <svg preserveAspectRatio="none" style="isolation:isolate" viewBox="0 0 1569 34">
                    <g clip-path="url(#clipPath851)" id="g9" transform="rotate(180,784.5,17.04549)">
                        <path fill="<?php echo $backgroundColour; ?>" id="path7" d="M 210.393,0 0,10.295 v 5.144 324.249 11.765 l 161.466,-6.62 104.381,5.15 192.46,-5.15 156.569,5.15 550.456,1.47 99.49,-11.765 304.178,5.145 V 5.145 L 1352.895,10.295 1268.898,8.09 1071.553,4.41 998.16,10.295 959.014,4.41 503.159,13.969 344.138,5.145 Z" />
                    </g>
                </svg>
            </div>
        <?php } ?>


    </div>
</div>
