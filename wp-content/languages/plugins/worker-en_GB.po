# Translation of Plugins - ManageWP Worker - Development (trunk) in English (UK)
# This file is distributed under the same license as the Plugins - ManageWP Worker - Development (trunk) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2020-04-15 18:40:44+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/3.0.0-alpha.2\n"
"Language: en_GB\n"
"Project-Id-Version: Plugins - ManageWP Worker - Development (trunk)\n"

#: src/MWP/EventListener/PublicRequest/AutomaticLogin.php:140
msgid "Invalid site connection specified. Please try again, or, if this keeps happening, contact support."
msgstr "Invalid site connection specified. Please try again, or if this keeps happening, contact support."

#: functions.php:878
msgid "Every 5 Minutes"
msgstr "Every 5 minutes"

#: src/MWP/EventListener/PublicRequest/AutomaticLogin.php:154
msgid "The automatic login token isn't properly signed. Please contact our support for help."
msgstr "The automatic log in token isn't properly signed. Please contact our support for help."

#: functions.php:1150
msgid "The site is currently in maintenance mode."
msgstr "The site is currently in maintenance mode."

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:396
msgid "Currently loaded keys:"
msgstr "Currently loaded keys:"

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:379
msgid "Copy"
msgstr "Copy"

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:371
msgid "Connection key:"
msgstr "Connection key:"

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:358
msgid "Disconnect"
msgstr "Disconnect"

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:339
#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:351
msgid "N/A"
msgstr "N/A"

#. translators: the variable is going to contain a human time difference string
#. like "2 days"
#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:336
#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:348
msgid "%s ago"
msgstr "%s ago"

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:323
msgid "Last Used"
msgstr "Last used"

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:319
msgid "Connected"
msgstr "Connected"

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:315
msgid "ID"
msgstr "ID"

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:308
msgid "Here is the list of currently active connections to this plugin:"
msgstr "Here is the list of currently active connections to this plugin:"

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:301
msgid "Enter this website's URL. When prompted, paste the connection key."
msgstr "Enter this website's URL. When prompted, paste the connection key."

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:287
msgid "Copy the connection key below."
msgstr "Copy the connection key below."

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:283
msgid "Install and activate the <b>Worker</b> plugin."
msgstr "Install and activate the <b>Worker</b> plugin."

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:278
msgid "Manual"
msgstr "Manual"

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:272
msgid "Enter this website's URL, admin username and password, and the system will take care of everything."
msgstr "Enter this website's URL, admin username and password, and the system will take care of everything."

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:255
msgid "Automatic"
msgstr "Automatic"

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:250
msgid "There are two ways to connect your website to the management dashboard:"
msgstr "There are two ways to connect your website to the management dashboard:"

#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:203
#: src/MWP/EventListener/PublicRequest/AddConnectionKeyInfo.php:433
msgid "Connection Management"
msgstr "Connection management"

#. Author URI of the plugin
msgid "https://godaddy.com"
msgstr "https://godaddy.com"

#. Author of the plugin
msgid "GoDaddy"
msgstr "GoDaddy"

#. Description of the plugin
msgid "We help you efficiently manage all your WordPress websites. <strong>Updates, backups, 1-click login, migrations, security</strong> and more, on one dashboard. This service comes in two versions: standalone <a href=\"https://managewp.com\">ManageWP</a> service that focuses on website management, and <a href=\"https://godaddy.com/pro\">GoDaddy Pro</a> that includes additional tools for hosting, client management, lead generation, and more."
msgstr "We help you efficiently manage all your WordPress websites. <strong>Updates, backups, 1-click login, migrations, security</strong> and more, on one dashboard. This service comes in two versions: standalone <a href=\"https://managewp.com\">ManageWP</a> service that focuses on website management, and <a href=\"https://godaddy.com/pro\">GoDaddy Pro</a> that includes additional tools for hosting, client management, lead generation, and more."

#. Plugin URI of the plugin
msgid "https://managewp.com"
msgstr "https://managewp.com"

#. Plugin Name of the plugin
msgid "ManageWP - Worker"
msgstr "ManageWP - Worker"

#. translators: the variable in this string is the WordPress username that
#. could not be found
#: src/MWP/EventListener/PublicRequest/AutomaticLogin.php:165
msgid "User <strong>%s</strong> could not be found."
msgstr "User <strong>%s</strong> could not be found."

#: src/MWP/EventListener/PublicRequest/AutomaticLogin.php:158
msgid "The automatic login token is invalid. Please check if this website is properly connected with your dashboard, or, if this keeps happening, contact support."
msgstr "The automatic login token is invalid. Please check if this website is properly connected with your dashboard, or, if this keeps happening, contact support."

#: src/MWP/EventListener/PublicRequest/AutomaticLogin.php:128
msgid "The automatic login token was already used. Please try again, or, if this keeps happening, contact support."
msgstr "The automatic login token was already used. Please try again, or, if this keeps happening, contact support."

#: src/MWP/EventListener/PublicRequest/AutomaticLogin.php:126
msgid "The automatic login token has expired. Please try again, or, if this keeps happening, contact support."
msgstr "The automatic login token has expired. Please try again, or, if this keeps happening, contact support."

#: src/MWP/EventListener/PublicRequest/AutomaticLogin.php:124
msgid "The automatic login token is invalid. Please try again, or, if this keeps happening, contact support."
msgstr "The automatic login token is invalid. Please try again, or, if this keeps happening, contact support."